# fluent mock简介
fluent mock是一款利用instrument(java agent)和 annotation processor这2个java提供的黑科技实现测试过程
中mock Java类/接口/对象的Mock工具。

## 开始使用fluent-mock
### [环境配置](docs/mock-config.md)
### [fluent mock功能一瞥](docs/mockup-base.md)
### [MockUp具体使用介绍](docs/mockup-advanced.md)
### [走上fluent之路](docs/fluent-mock.md)

## [和其它mock框架的比较](docs/compare-other.md)