# 配置

## maven配置
- jar包引用
在项目pom.xml文件中
```xml
<project>
    <properties>
        <fluent-mock.version>1.0.5</fluent-mock.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.test4j</groupId>
            <artifactId>fluent-mock</artifactId>
            <version>${fluent-mock.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>
```
- 和surefire 
和maven-surefire-plugin插件一起使用，需要在 argLine 参数配置参数
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <configuration>
                <argLine>-javaagent:${settings.localRepository}/org/test4j/fluent-mock/${fluent-mock.version}/fluent-mock-${fluent-mock.version}.jar</argLine>
            </configuration>
        </plugin>
    </plugins>
</build>
```
- 和surefire + jacoco一起
如果同时使用了jacoco的来统计单元测试覆盖率，则需在<argLine>配置中添加一个@{argLine}
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <configuration>
                <argLine>@{argLine} -javaagent:${settings.localRepository}/org/test4j/fluent-mock/${fluent-mock.version}/fluent-mock-${fluent-mock.version}.jar</argLine>
            </configuration>
        </plugin>
        <plugin>
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <version>0.8.6</version>
            <executions>
                <execution>
                    <id>default-prepare-agent</id>
                    <goals>
                        <goal>prepare-agent</goal>
                    </goals>
                </execution>
                <execution>
                    <id>default-report</id>
                    <goals>
                        <goal>report</goal>
                    </goals>
                    <phase>test</phase>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

## gradle配置
```groovy
dependencies {
    testCompile('org.test4j:fluent-mock:${fluent-mock.version}')
    //annotationProcessor('org.test4j:fluent-mock:${fluent-mock.version}')
    testAnnotationProcessor('org.test4j:fluent-mock:${fluent-mock.version}')

    test {
        jvmArgs "-javaagent:${classpath.find { it.name.contains("fluent-mock") }.absolutePath}"
        useJUnitPlatform()
    }
}

```
```groovy
    compileOnly "org.projectlombok:lombok:${lombokVersion}"
    annotationProcessor "org.projectlombok:lombok:${lombokVersion}"
    compileOnly("org.test4j:fluent-mock:1.0.5")
    annotationProcessor("org.test4j:fluent-mock:1.0.5")

    testCompile "org.projectlombok:lombok:${lombokVersion}"
    testAnnotationProcessor "org.projectlombok:lombok:${lombokVersion}"
    testCompile("org.test4j:fluent-mock:1.0.5")
    testAnnotationProcessor("org.test4j:fluent-mock:1.0.5")

    test {
        jvmArgs "-javaagent:${classpath.find { it.name.contains("fluent-mock") }.absolutePath}"
        useJUnitPlatform()
    }
```

## IDE配置
大部分情况下，IDE会自动识别maven和gradle的配置，如果mock框架没有起作用，可以在运行测试的jvm参数中加上：
```text
-javaagent:{你本地仓库根路径}/org/test4j/fluent-mock/1.0.5/fluent-mock-1.0.5.jar
```
如果是使用maven, 也可以设置成
```text
-javaagent:$MAVEN_REPOSITORY$/org/test4j/fluent-mock/1.0.5/fluent-mock-1.0.5.jar
```