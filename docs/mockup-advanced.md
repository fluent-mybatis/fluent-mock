# MockUp具体使用介绍
为了讲述如何使用MockUp，我们先给出业务类，这个类有static,final,private,public等方法。
```java
/**
 * 一个普通的业务类, 有各式限定方法
 */
@Getter
public class AnNormalService {
    /**
     * 静态变量
     */
    private static int staticInt;
    /**
     * 成员变量
     */
    private int anInt;

    /**
     * 静态代码块
     */
    static {
        setStaticInt(200);
    }

    /**
     * 构造函数
     *
     * @param anInt
     */
    public AnNormalService(int anInt) {
        this.anInt = anInt;
    }

    /**
     * 静态方法
     *
     * @param num
     */
    public static void setStaticInt(int num) {
        staticInt = num;
    }

    public static int getStaticInt() {
        return staticInt;
    }

    /**
     * 普通方法
     *
     * @return
     */
    public int publicMethod() {
        return this.privateMethod();
    }

    /**
     * private方法
     *
     * @return
     */
    private int privateMethod() {
        return anInt;
    }

    /**
     * final方法
     *
     * @return
     */
    public final int finalMethod() {
        return this.protectedMethod();
    }

    /**
     * protected方法
     *
     * @return
     */
    private int protectedMethod() {
        return anInt;
    }
}
```

## 各种限定类型方法mock演示
### 对静态代码块的mock
静态代码块对应的mock方法名为 $clinit(), 无参数
```java
public class AnNormalServiceMockUpTest {
    @BeforeAll
    static void setup() {
        new MockUp<AnNormalService>() {
            /**
             * 对静态代码块的mock
             */
            @Mock
            void $clinit() {
                AnNormalService.setStaticInt(300);
            }
        };

    }

    @DisplayName("验证静态代码块mock")
    @Test
    void test1() {
        int anInt = AnNormalService.getStaticInt();
        Assertions.assertEquals(300, anInt);
    }
}
```

### 对构造函数进行mock
要mock构造函数，使用方法名 $init, 具体的入参同构造函数, 为了引用被mock的实例对象, 可以在入参列表前增加入参Invocation。
```java
public class AnNormalServiceMockUpTest {
    
    @DisplayName("对构造函数进行mock")
    @Test
    void test2() {
        new MockUp<AnNormalService>() {
            @Mock
            void $init(Invocation inv, int anInt) {
                AnNormalService self = inv.getTarget();
                /**
                 * 成员变量anInt = 输入值 + 200
                 */
                self.setAnInt(anInt + 200);
            }
        };
        int anInt = new AnNormalService(200).publicMethod();
        Assertions.assertEquals(400, anInt);
    }
}
```

### mock public和final方法
对任意方法的mock很简单，在MockUp类里面定义同名函数，入参类型一致，就可以改变方法的行为。
```java
public class AnNormalServiceMockUpTest {
    
    @DisplayName("演示对public和final方法mock")
    @Test
    void test3() {
        new MockUp<AnNormalService>() {
            @Mock
            int publicMethod() {
                return 5;
            }

            @Mock
            int finalMethod() {
                return 6;
            }
        };
        AnNormalService service = new AnNormalService(400);
        Assertions.assertEquals(5, service.publicMethod());
        Assertions.assertEquals(6, service.finalMethod());
    }


    @DisplayName("演示对private和protected方法mock")
    @Test
    void test4() {
        new MockUp<AnNormalService>() {
            @Mock
            int privateMethod() {
                return 9;
            }

            @Mock
            int protectedMethod() {
                return 11;
            }
        };
        AnNormalService service = new AnNormalService(400);
        Assertions.assertEquals(9, service.publicMethod());
        Assertions.assertEquals(11, service.finalMethod());
    }   
 
    @DisplayName("演示对静态方法的mock")
    @Test
    void test5() {
        new MockUp<AnNormalService>() {
            @Mock
            int getStaticInt() {
                return 178;
            }
        };
        Assertions.assertEquals(178, AnNormalService.getStaticInt());
    }
}
```

## 使用Invocation来指向mock实例对象
FluentMock提供了Invocation类型，可以作为mock方法的第一个参数。
Invocation提供了下面方法供mock使用:

```java
/**
 * Invocation类型可以作为mock方法的第一个参数
 */
public abstract class Invocation {
    /**
     * 执行方法原有的逻辑
     *
     * @param args 传递给方法的参数(可以是原始入参, 也可以是mock逻辑中篡改过的参数)
     */
    public <T> T proceed(Object... args);

    /**
     * 执行方法mock前的逻辑, 入参是原始入参
     */
    public <T> T proceed();

    /**
     * 当前执行实例(即相当于Java内置的变量this), 静态方法返回null
     */
    public abstract <T> T getTarget();

    /**
     * 获得方法的入参列表
     */
    public abstract Object[] getArgs();

    /**
     * 获得第index个参数, index从0开始
     */
    public Object arg(int index);

    /**
     * 获得第index个参数, index从0开始; 并强制转换为类型ARG
     *
     * @param index
     * @param clazz 强转类型
     */
    public <ARG> ARG arg(int index, Class<ARG> clazz);

    /**
     * 返回方法是被第几次调用
     */
    public abstract int getInvokedTimes();
}
```

Invocation在mock中作用很大，可以做很多事情

### 执行原逻辑
比如有业务类如下:
```java
public class Service {
    private String value = "origin string";

    public String getString() {
        return value;
    }

    public void setString(String input) {
        this.value = input;
    }
}
```
mock方法getString，我们希望在原有的返回值上面追加信息, 可以像下面这样mock。
```java
public class InvocationDemo {
    @DisplayName("演示Mock方法执行原方法逻辑")
    @Test
    void test1() {
        new MockUp<Service>() {
            @Mock
            String getString(Invocation inv) {
                String origin = inv.proceed();
                return origin + ", plus mock info.";
            }
        };
        String result = new Service().getString();
        Assertions.assertEquals("origin string, plus mock info.", result);
    }
}
```

### 按调用时序进行mock

有时候，我们对同一方法调用时，需要根据调用时序返回不同的结果。
比如，还是对上面的getString就行mock，希望前3次返回不同的值，其余的返回原方法返回值。

```java
public class InvocationDemo {
    @DisplayName("演示按调用时序进行mock")
    @Test
    void test2() {
        new MockUp<Service>() {
            @Mock
            String getString(Invocation inv) {
                switch (inv.getInvokedTimes()) {
                    case 1: return "mock 1";
                    case 2: return "mock 2";
                    case 3: return "mock 3";
                    default: return inv.proceed();
                }
            }
        };
        Service service = new Service();
        assertEquals("mock 1", service.getString());
        assertEquals("mock 2", service.getString());
        assertEquals("mock 3", service.getString());
        assertEquals("origin string", service.getString());
        assertEquals("origin string", service.getString());
    }
}
```

## 对入参或出参进行断言
在测试过程中，除了需要对外部接口进行mock外。有时候还需要对入参和返回结果值进行判断，来验证我们的程序执行是否正常。
### 对入参进行断言

```java
public class InvocationDemo {
    @DisplayName("对方法入参进行断言演示")
    @Test
    void test3() {
        new MockUp<Service>() {
            @Mock
            void setString(Invocation inv, String input) {
                Assertions.assertEquals("期望值", input);
                inv.proceed();
            }
        };
        Service service = new Service();
        // 正常通过
        service.setString("期望值");
        // 设置其它值, 应该是抛出一个断言异常
        Assertions.assertThrows(AssertionError.class, () -> service.setString("其它值"));
    }
}
```

### 对方法出参进行断言

```java
public class InvocationDemo {
    @DisplayName("对方法出参进行断言演示")
    @Test
    void test4() {
        new MockUp<Service>() {
            @Mock
            String getString(Invocation inv) {
                String result = inv.proceed();
                Assertions.assertEquals("origin string", result);
                return result;
            }
        };
        Service service = new Service();
        /**
         * 方法调用正常通过
         */
        service.getString();
        /**
         * 将返回值改为其它值, 方法调用应该抛出断言异常
         */
        service.setString("other value");
        Assertions.assertThrows(AssertionError.class, () -> service.getString());
    }
}
```

## 对指定的对象实例进行Mock
上面我们演示的普适性的Mock, 及Mock的方法对所有实例生效（因为fluent mock修改的是类字节码实现）,任何执行到对应方法的调用，都会进入mock后的逻辑。
但如果我们只想对特定的对象实例进行mock如何处理呢？
比如我们有个User类, User有2个成员变量：配偶和小孩列表.

```java
@Data
@Accessors(chain = true)
public class User {
    private String name;

    private User spouse;

    private List<User> children = new ArrayList<>();

    public User(String name) {
        this.name = name;
    }

    public User addChild(User child) {
        children.add(child);
        return this;
    }

    /**
     * 自我介绍
     *
     * @return
     */
    public String sayHi() {
        StringBuilder buff = new StringBuilder();
        buff.append("I'm ").append(this.getName());
        if (this.spouse != null) {
            buff.append(" and my spouse is ").append(this.spouse.getName()).append(".");
        }
        if (!this.children.isEmpty()) {
            buff.append("I have ").append(children.size()).append(" children, ")
                .append(this.children.stream().map(User::getName).collect(Collectors.joining(" and ")))
                .append(".");
        }
        return buff.toString();
    }
}
```

现在我们演示一下如何对指定对象进行mock, 我们只需要把需要mock的对象实例（或实例列表）当作MockUp的构造参数就可以了。这样对应的MockUp里面
定义的@Mock方法就只对入参的实例有效，而对其它实例则没有效果。
现在我们演示一下如何对指定对象进行mock

```java
public class TargetObjectMockDemo {

    @DisplayName("对指定实例进行mock演示")
    @Test
    void test1() {
        /** 有一个配偶, 2个小孩 **/
        User user = new User("tom")
            .setSpouse(new User("mary"))
            .addChild(new User("mike"))
            .addChild(new User("jack"));
        String hi = user.sayHi();
        /** mock前的自我介绍 **/
        assertEquals("I'm tom and my spouse is mary.I have 2 children, mike and jack.", hi);
        /**
         * 对配偶进行mock
         */
        new MockUp<User>(user.getSpouse()) {
            @Mock
            String getName(Invocation inv) {
                return "virtual " + inv.proceed();
            }
        };
        /**
         * 对小孩进行mock
         */
        new MockUp<User>(user.getChildren()) {
            @Mock
            String getName(Invocation inv) {
                return "fictitious " + inv.proceed();
            }
        };
        hi = user.sayHi();
        /** mock后的自我介绍 **/
        assertEquals("I'm tom and my spouse is virtual mary.I have 2 children, fictitious mike and fictitious jack.", hi);
    }
}
```

通过示例，我们看到"virtual"修饰只对配偶实例getName()方法起作用, "fictitious"修饰只对小孩的getName()起作用, 而user本尊的getName()没有受到影响。