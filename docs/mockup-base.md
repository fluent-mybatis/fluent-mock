## fluent mock功能一瞥
### MockUp & @Mock提供的Mock方式，直接简单。
定义一个匿名类 MockUp, 其泛型参数是要mock的类，在匿名类实现里面，使用@Mock指明要mock的方法，
方法的签名和实际方法实现一致。
示例：
```java
public class NormalMockUpTest {
    @DisplayName("对Java自带类Calendar的get方法进行定制")
    @Test
    public void testMockUp() {
        /**
         * 只需要把Calendar类传入MockUp类的构造函数即可
         */
        new MockUp<Calendar>(Calendar.class) {
            /**
             * 想Mock哪个方法，就给哪个方法加上@Mock，没有@Mock的方法，不受影响
             */
            @Mock
            public int get(int unit) {
                if (unit == Calendar.YEAR) {
                    return 2017;
                }
                if (unit == Calendar.MONDAY) {
                    return 12;
                }
                if (unit == Calendar.DAY_OF_MONTH) {
                    return 25;
                }
                if (unit == Calendar.HOUR_OF_DAY) {
                    return 7;
                }
                return 0;
            }
        };
        // 从此Calendar的get方法，就沿用你定制过的逻辑，而不是它原先的逻辑。
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        Assertions.assertTrue(cal.get(Calendar.YEAR) == 2017);
        Assertions.assertTrue(cal.get(Calendar.MONDAY) == 12);
        Assertions.assertTrue(cal.get(Calendar.DAY_OF_MONTH) == 25);
        Assertions.assertTrue(cal.get(Calendar.HOUR_OF_DAY) == 7);
        // Calendar的其它方法，不受影响
        Assertions.assertTrue((cal.getFirstDayOfWeek() == Calendar.MONDAY));
    }
}
```

### 使用Fluent API方式
要使用fluent方式，在测试类上加注解@Mocks, 框架会帮忙在编译时生成(定义Mocks类名+Mocks)命名的指导类。
使用指导类变量就可以对要mock的类进行行为定义。
```java
/**
 * 指明在fluent接口中要mock的类, Annotation Processor会根据@Mocks定义
 * 生成Test类+Mocks命名的指导类, 在这里是：FluentMockUpTestMocks
 */
@Mocks({Calendar.class})
public class FluentMockUpTest {
    private FluentMockUpTestMocks mocks = new FluentMockUpTestMocks();

    @DisplayName("使用fluent方式对自带类Calendar的get方法进行定制")
    @Test
    public void testMockUp() {
        mocks.Calendar(faker -> {
            /**
             * 指定要mock get方法, 同时实现get的mock逻辑
             */
            faker.get.restAnswer(inv -> {
                int unit = inv.arg(0);
                switch (unit) {
                    case Calendar.YEAR:
                        return 2017;
                    case Calendar.MONDAY:
                        return 12;
                    case Calendar.DAY_OF_MONTH:
                        return 25;
                    case Calendar.HOUR_OF_DAY:
                        return 7;
                    default:
                        return 0;
                }
            });
        });
        // 从此Calendar的get方法，就沿用你定制过的逻辑，而不是它原先的逻辑。
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        Assertions.assertTrue(cal.get(Calendar.YEAR) == 2017);
        Assertions.assertTrue(cal.get(Calendar.MONDAY) == 12);
        Assertions.assertTrue(cal.get(Calendar.DAY_OF_MONTH) == 25);
        Assertions.assertTrue(cal.get(Calendar.HOUR_OF_DAY) == 7);
        // Calendar的其它方法，不受影响
        Assertions.assertTrue((cal.getFirstDayOfWeek() == Calendar.MONDAY));
    }
}
```
