- v1.1.6
    1. 修复v1.1.5生命周期管理中测试结束清理, FluentMockUp#registered未重置，导致mock失效

- v1.1.5
    1. FluentMockUp#behavior 生命周期管理改进, 避免 XyzMocks.mocks中定义的mock行为在多个测试中交叉影响

- v1.1.4
    1. 删除DataProvider，类挪到fluent-assert

- v1.1.3
    1. 增加 @SpringContext 注解功能

- v1.1.2
    1. 替换dubbo @Reference时，对报错信息进行优化

- v1.1.1
    1. 从BeanRegister类中拆出BeanRegisterProcessor类, 避免BeanRegister对spring的直接依赖, 导致gradle环境下annotation processor对spring-bean
       jar的依赖
    2. 优化spring bean替换策略

- v1.1.0
    1. Feature: BeanRegister功能, 增加自定义spring bean功能
    2. 根据@Mocks names和beans属性值, 生成 TestedClassStubRegister spring配置类, 在spring启动时加载
    3. 根据@Beans names和beans属性值, 生成 TestedClassBeanRegister spring配置类, 在spring启动时加载
  
