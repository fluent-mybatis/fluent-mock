package org.test4j;

/**
 * Logger
 *
 * @author wudarui
 */
@SuppressWarnings({"unused"})
public class Logger {
    public static final int DEBUG = 0;

    public static final int INFO = 1;

    public static final int WARNING = 2;

    public static final int ERROR = 3;

    public static int level = DEBUG;

    private static void mark(int level, String marker) {
        switch (level) {
            case ERROR:
                System.err.println(marker);
                break;
            case WARNING:
            default:
                System.out.println(marker);
        }
    }

    public static void debug(Object info, Throwable... throwables) {
        if (level <= DEBUG) {
            mark(DEBUG, "DEBUG: " + info);
            printExceptions(throwables);
        }
    }

    public static void warn(Object warn, Throwable... throwables) {
        if (level <= WARNING) {
            mark(WARNING, "WARNING: " + warn);
            printExceptions(throwables);
        }
    }

    public static void info(Object info, Throwable... throwables) {
        if (level <= INFO) {
            mark(INFO, "INFO: " + info);
            printExceptions(throwables);
        }
    }

    public static void error(Object err, Throwable... throwables) {
        mark(ERROR, "ERROR: " + err);
        printExceptions(throwables);
    }

    private static void printExceptions(Throwable[] throwables) {
        if (throwables == null || throwables.length == 0) {
            return;
        }
        for (Throwable e : throwables) {
            e.printStackTrace();
        }
    }
}