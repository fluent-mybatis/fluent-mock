package org.test4j.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 在spring context初始前执行的方法<br>
 * 注解只有声明在 public static 限定修饰方法上才可以生效
 *
 * @author darui.wudr
 */
@Target(ElementType.METHOD)
@Retention(RUNTIME)
public @interface BeforeSpring {
}