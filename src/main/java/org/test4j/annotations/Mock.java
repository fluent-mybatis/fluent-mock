package org.test4j.annotations;

import org.test4j.mock.Invocation;
import org.test4j.mock.MockUp;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 在 {@linkplain MockUp}类里面定义mock方法
 * mock 方法名称和参数列表必须与原方法一致（mock方法第一个参数可以是 {@link Invocation}）
 * 方法的限定符不管原方法是static/public/private/protected/package, mock方法都是public
 * <p>
 * 要mock构造函数, mock方法名称: $init
 * 要mock静态代码块, mock方法名称: $clinit
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Mock {
}