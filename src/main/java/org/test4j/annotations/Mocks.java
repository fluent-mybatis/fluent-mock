package org.test4j.annotations;

import org.springframework.context.annotation.Import;
import org.test4j.integration.spring.registrar.MocksScanRegistrar;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 在测试类中快速定义 MockUp 类, 代码编译生成
 *
 * @author wudarui
 */
@Documented
@Retention(RUNTIME)
@Target(ElementType.TYPE)
@Import(MocksScanRegistrar.class)
public @interface Mocks {
    /**
     * 仅生成mock proxy引导类
     */
    Class[] value() default {};

    /**
     * 除了生成mock proxy引导类, 同时生成Spring fake bean config配置类
     */
    Class[] beans() default {};

    /**
     * 指定名称的stub bean, 同时生成Spring fake bean config配置类
     */
    Named[] names() default {};

    /**
     * 生成的MocksApply类后缀
     */
    String suffixMocks() default "Mocks";
}