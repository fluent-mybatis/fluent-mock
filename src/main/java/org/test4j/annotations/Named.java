package org.test4j.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 对stub bean name定义
 */
@SuppressWarnings("rawtypes")
@Retention(RUNTIME)
@Target(ANNOTATION_TYPE)
public @interface Named {
    /**
     * bean名称
     */
    String name();

    /**
     * bean实现类
     */
    Class type();
}