package org.test4j.integration.junit4;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * DataFrom
 *
 * @author darui.wu
 */
@Retention(RUNTIME)
@Target({METHOD})
@SuppressWarnings("rawtypes")
public @interface DataFrom {
    String value() default "";

    Class clazz() default DataFrom.class;
}