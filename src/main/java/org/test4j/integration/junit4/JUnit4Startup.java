package org.test4j.integration.junit4;

import org.test4j.integration.junit4.faker.FakeFrameworkMethod;
import org.test4j.integration.junit4.faker.FakeJUnit4Builder;
import org.test4j.integration.junit4.faker.FakeParentRunner;
import org.test4j.integration.junit4.faker.FakeRunNotifier;
import org.test4j.mock.faking.util.ClassLoad;
import org.test4j.mock.startup.IStartupMock;


/**
 * 使用JUnit4框架时初始化
 *
 * @author darui.wu
 */
public class JUnit4Startup implements IStartupMock {

    final String FrameworkMethod_Package = "org.junit.runners.model.FrameworkMethod";

    final String JupiterExtension_Package = "org.junit.jupiter.api.extension.Extension";

    final String Jupiter_Enabled_Package = "junit.jupiter.extensions.autodetection.enabled";

    @Override
    public void initial() {
        if (ClassLoad.isInClasspath(FrameworkMethod_Package)) {
            new FakeJUnit4Builder();
            new FakeParentRunner();
            new FakeRunNotifier();
            new FakeFrameworkMethod();
        }
        if (ClassLoad.isInClasspath(JupiterExtension_Package)) {
            System.setProperty(Jupiter_Enabled_Package, "true");
        }
    }
}