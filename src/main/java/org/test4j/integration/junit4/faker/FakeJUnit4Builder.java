package org.test4j.integration.junit4.faker;

import org.junit.internal.builders.JUnit4Builder;
import org.junit.runner.Runner;
import org.test4j.annotations.Mock;
import org.test4j.integration.junit4.runner.ProxyRunner;
import org.test4j.mock.MockUp;
import org.test4j.module.spring.SpringEnv;

import static org.test4j.mock.faking.util.ReflectUtility.doThrow;

/**
 * FakeJUnit4Builder
 *
 * @author wudarui
 */
@SuppressWarnings("unused")
public class FakeJUnit4Builder extends MockUp<JUnit4Builder> {
    /**
     * 覆盖junit原生执行器
     *
     * @param testClass test class
     * @return ignore
     */
    @Mock
    public Runner runnerForClass(Class<?> testClass) throws Throwable {
        try {
            SpringEnv.setSpringEnv(testClass);
            return ProxyRunner.getRunner(testClass);
        } catch (Throwable e) {
            e.printStackTrace();
            return doThrow(e);
        }
    }
}