package org.test4j.integration.junit4.faker;

import org.junit.runner.manipulation.Filter;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.TestClass;
import org.test4j.annotations.Mock;
import org.test4j.integration.ListenerFactory;
import org.test4j.integration.junit4.helper.FrameworkMethodWithParameters;
import org.test4j.mock.Invocation;
import org.test4j.mock.MockUp;

/**
 * FakeParentRunner
 *
 * @author wudarui
 */
@SuppressWarnings({"unused", "rawtypes"})
public class FakeParentRunner extends MockUp<ParentRunner> {

    @Mock
    public TestClass createTestClass(Invocation inv, Class testClass) {
        ListenerFactory.beforeAll(testClass);
        return inv.proceed();
    }

    @Mock
    public boolean shouldRun(Invocation inv, Filter filter, Object each) {
        String desc = filter.describe();
        if (each instanceof FrameworkMethodWithParameters) {
            int index = each.toString().indexOf("[");
            String name = each.toString().substring(0, index);
            return desc.contains(name + "(");
        } else {
            return inv.proceed();
        }
    }
}