package org.test4j.integration.junit4.faker;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.RunNotifier;
import org.test4j.annotations.Mock;
import org.test4j.integration.ListenerFactory;
import org.test4j.mock.Invocation;
import org.test4j.mock.MockListener;
import org.test4j.mock.MockUp;

/**
 * 和 {@link FakeFrameworkMethod}一起完成完整的{@link MockListener}功能
 *
 * @author darui.wu
 */
@SuppressWarnings({"unused"})
public final class FakeRunNotifier extends MockUp<RunNotifier> {
    @Mock
    public static void fireTestRunStarted(Invocation inv, Description description) {
        ListenerFactory.beforeAll(description.getTestClass());
        inv.proceed();
    }

    @Mock
    public static void fireTestRunFinished(Invocation inv, Result result) {
        ListenerFactory.afterAll();
        inv.proceed();
    }
}