package org.test4j.integration.junit4.helper;

import org.junit.runners.model.FrameworkMethod;
import org.test4j.integration.junit4.DataFrom;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings({"rawtypes", "unchecked"})
public class DataFromHelper {
    /**
     * 构造一系列有参的测试方法
     *
     * @param testClazz  当前测试类class
     * @param testMethod 测试方法
     * @param dataFrom   DataFrom
     * @return ignore
     */
    public static List<FrameworkMethod> computeParameterizedTestMethods(Class testClazz,
                                                                        Method testMethod,
                                                                        DataFrom dataFrom) {
        String fromMethod = dataFrom.value();
        if ("".equals(fromMethod)) {
            throw new RuntimeException("You should specify the value property of @DataFrom() item.");
        }

        Class dataFromClazz = dataFrom.clazz();
        if (dataFromClazz == DataFrom.class) {
            dataFromClazz = testMethod.getDeclaringClass();
        }
        return computeParameterizedFromDataProviderMethod(testClazz, testMethod, fromMethod, dataFromClazz);
    }

    private static List<FrameworkMethod> computeParameterizedFromDataProviderMethod(Class testClazz,
                                                                                    Method testMethod,
                                                                                    String dataFromMethod,
                                                                                    Class dataFromClaz) {
        Object data = getDataFromMethod(dataFromMethod, testClazz, dataFromClaz);
        if (data instanceof Iterator) {
            return computeParameterFromIterator(testMethod, (Iterator) data);
        } else if (data instanceof Object[][]) {
            return computeParameterFromArray(testMethod, (Object[][]) data);
        } else {
            throw new RuntimeException(
                "The @DataFrom method can only return value of type Iterator<Object[]> or Object[][].");
        }
    }

    private static Object getDataFromMethod(String dataFromMethod, Class testClazz, Class dataFromClazz) {
        try {
            Class clazz = dataFromClazz;
            if (dataFromClazz.isAssignableFrom(testClazz)) {
                clazz = testClazz;
            }
            Method method = clazz.getDeclaredMethod(dataFromMethod);
            method.setAccessible(true);
            if (Modifier.isStatic(method.getModifiers())) {
                return method.invoke(null);
            } else {
                Object target = clazz.newInstance();
                return method.invoke(target);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static List<FrameworkMethod> computeParameterFromIterator(Method method, Iterator iterator) {
        List<FrameworkMethod> methodWithParameters = new ArrayList<>();
        while (iterator.hasNext()) {
            Object caseData = iterator.next();
            if (caseData instanceof Object[]) {
                methodWithParameters.add(new FrameworkMethodWithParameters(method, (Object[]) caseData));
            } else {
                methodWithParameters.add(new FrameworkMethodWithParameters(method, new Object[]{caseData}));
            }
        }
        return methodWithParameters;
    }

    private static List<FrameworkMethod> computeParameterFromArray(Method method, Object[][] array) {
        List<FrameworkMethod> methodWithParameters = new ArrayList<>();
        for (Object[] caseData : array) {
            methodWithParameters.add(new FrameworkMethodWithParameters(method, caseData));
        }
        return methodWithParameters;
    }
}