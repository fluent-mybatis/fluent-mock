package org.test4j.integration.junit4.helper;


import org.junit.runners.model.FrameworkMethod;
import org.test4j.integration.junit4.DataFrom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * JUnit框架方法装饰
 *
 * @author wudarui
 */
@SuppressWarnings("rawtypes")
public class FrameworkParameterMethodHelper {

    /**
     * 将junit测试方法转换为test4j测试方法
     *
     * @param testClass test class
     * @param methods   test methods
     * @return ignore
     */
    public static List<FrameworkMethod> computeTestParaMethods(Class testClass, List<FrameworkMethod> methods) {
        List<FrameworkMethod> list = new ArrayList<>();
        for (FrameworkMethod method : methods) {
            List<FrameworkMethod> pms = FrameworkParameterMethodHelper.withParameter(testClass, method);
            list.addAll(pms);
        }
        return list;
    }

    private static List<FrameworkMethod> withParameter(Class testClass, FrameworkMethod method) {
        DataFrom dataFrom = method.getMethod().getAnnotation(DataFrom.class);
        if (dataFrom == null) {
            return Collections.singletonList(method);
        } else {
            return DataFromHelper.computeParameterizedTestMethods(testClass, method.getMethod(), dataFrom);
        }
    }
}