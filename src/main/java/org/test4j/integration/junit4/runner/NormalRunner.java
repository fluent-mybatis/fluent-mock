package org.test4j.integration.junit4.runner;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import java.lang.annotation.Annotation;
import java.util.List;

import static org.test4j.integration.junit4.helper.FrameworkParameterMethodHelper.computeTestParaMethods;

/**
 * NormalRunner
 *
 * @author wudarui
 */
@SuppressWarnings("rawtypes")
public class NormalRunner extends BlockJUnit4ClassRunner implements ProxyRunner {
    public NormalRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    public List<FrameworkMethod> computeTestMethods() {
        Class testClass = this.getTestClass().getJavaClass();
        return computeTestParaMethods(testClass, super.computeTestMethods());
    }

    @Override
    public void validatePublicVoidNoArgMethods(Class<? extends Annotation> annotation, boolean isStatic, List<Throwable> errors) {
        this.validateMethod(annotation, isStatic, errors);
    }
}