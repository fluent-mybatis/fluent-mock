package org.test4j.integration.junit4.runner;

import org.junit.Test;
import org.junit.runner.Runner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.TestClass;
import org.test4j.module.spring.SpringEnv;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * ProxyRunner
 *
 * @author wudarui
 */
public interface ProxyRunner {
    List<FrameworkMethod> computeTestMethods();

    TestClass getTestClass();

    /**
     * 验证测试方法有效性
     *
     * @param annotation Annotation
     * @param isStatic   is static
     * @param errors     error list
     */
    default void validateMethod(Class<? extends Annotation> annotation, boolean isStatic, List<Throwable> errors) {
        List<FrameworkMethod> methods = getTestClass().getAnnotatedMethods(annotation);
        if (annotation.isAssignableFrom(Test.class)) {
            methods = this.computeTestMethods();
        }
        for (FrameworkMethod eachTestMethod : methods) {
            eachTestMethod.validatePublicVoidNoArg(isStatic, errors);
        }
    }

    /**
     * 根据环境和类是否注解spring容器加载, 返回不同的Runner
     */
    static Runner getRunner(Class<?> testClass) throws InitializationError {
        if (SpringEnv.isSpringEnv(testClass)) {
            return SpringRunner.create(testClass);
        } else {
            return new NormalRunner(testClass);
        }
    }
}