package org.test4j.integration.junit4.runner;

import org.junit.runner.Runner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.test4j.module.spring.SpringInit;

import java.lang.annotation.Annotation;
import java.util.List;

import static org.test4j.integration.junit4.helper.FrameworkParameterMethodHelper.computeTestParaMethods;
import static org.test4j.mock.faking.util.ReflectUtility.doThrow;

/**
 * SpringRunner
 *
 * @author wudarui
 */
@SuppressWarnings("rawtypes")
public class SpringRunner extends SpringJUnit4ClassRunner implements ProxyRunner {

    public SpringRunner(Class<?> clazz) throws InitializationError {
        super(clazz);
    }

    protected Object createTest() throws Exception {
        Object target = super.createTest();
        SpringInit.doSpringInitial(target, this.getTestContextManager());
        return target;
    }

    @Override
    public List<FrameworkMethod> computeTestMethods() {
        Class testClass = this.getTestClass().getJavaClass();
        return computeTestParaMethods(testClass, super.computeTestMethods());
    }

    @Override
    public void validatePublicVoidNoArgMethods(Class<? extends Annotation> annotation, boolean isStatic, List<Throwable> errors) {
        this.validateMethod(annotation, isStatic, errors);
    }

    public static Runner create(Class aClass) {
        try {
            return new SpringRunner(aClass);
        } catch (InitializationError e) {
            return doThrow(e);
        }
    }
}