package org.test4j.integration.spring;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.test4j.mock.Stubs;

import java.util.HashMap;
import java.util.Map;

/**
 * 动态注册spring bean
 *
 * @author darui.wu
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class BeanRegister {
    /**
     * 注册 bean definition定义
     * <p>
     * key: bean name; value: bean class
     */
    final Map<String, Class> beans = new HashMap<>();
    /**
     * 直接替换spring bean实例
     * <p>
     * key: bean name; value: 伪造的实例
     */
    final Map<String, Object> faker = new HashMap<>();

    /**
     * 注册bean
     * <pre>
     *     <bean name = "name" class="klass"/>
     * </pre>
     *
     * @param name        bean name
     * @param beanOrKlass bean class or fake object
     * @return BeanRegister self
     */
    public BeanRegister bean(String name, Object beanOrKlass) {
        if (beanOrKlass == null) {
            return this;
        }
        if (beanOrKlass instanceof Class) {
            beans.put(name, (Class) beanOrKlass);
        } else {
            faker.put(name, beanOrKlass);
        }
        return this;
    }

    /**
     * 注册bean
     * <pre>
     *     <bean class="klass"/>
     * </pre>
     *
     * @param classes bean class
     * @return BeanRegister self
     */
    public BeanRegister bean(Class... classes) {
        for (Class klass : classes) {
            this.bean(camelName(klass), klass);
        }
        return this;
    }

    /**
     * 注册stub bean, 并且显式命名bean name
     *
     * @param name  bean name
     * @param klass stub class
     * @return BeanRegister self
     */
    public BeanRegister stub(String name, Class klass) {
        if (klass == null) {
            return this;
        }
        Object stub = Stubs.fake(klass);
        Stubs.nilFields(stub);
        faker.put(name, stub);
        return this;
    }

    /**
     * 注册stub bean
     *
     * @param classes stub class
     * @return BeanRegister self
     */
    public BeanRegister stub(Class... classes) {
        for (Class klass : classes) {
            this.stub(camelName(klass), klass);
        }
        return this;
    }

    /**
     * 驼峰命名法
     *
     * @param klass 类
     * @return 类名的驼峰结果
     */
    public static String camelName(Class klass) {
        String name = klass.getSimpleName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }

    public BeanFactoryPostProcessor processor() {
        return new BeanRegisterProcessor(this);
    }
}