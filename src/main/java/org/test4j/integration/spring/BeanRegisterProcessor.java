package org.test4j.integration.spring;

import lombok.experimental.Accessors;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.lang.NonNull;
import org.test4j.integration.spring.faker.StubFactoryBean;

import java.util.Map;

/**
 * spring bean definition注册
 * <p>
 * spring处理的逻辑和 BeanRegister 分开, 避免annotation processor对spring的直接依赖
 * 如果BeanRegister直接继承BeanDefinitionRegistryPostProcessor, 在annotation processor处理时会报类错误
 * 需要包spring-bean jar包也声明为annotationProcessor类型
 *
 * @author darui.wu
 */
@SuppressWarnings({"unused", "rawtypes"})
@Accessors(chain = true)
public class BeanRegisterProcessor implements BeanDefinitionRegistryPostProcessor {
    private final BeanRegister register;

    public BeanRegisterProcessor(BeanRegister register) {
        this.register = register;
    }

    public BeanRegisterProcessor(String registerClass) {
        try {
            this.register = (BeanRegister) Class.forName(registerClass).getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException("getRegister error:" + e.getMessage(), e);
        }
    }

    /**
     * 定义spring bean
     * <p>
     * {@inheritDoc}
     */
    @Override
    public void postProcessBeanDefinitionRegistry(@NonNull BeanDefinitionRegistry registry) throws BeansException {
        for (Map.Entry<String, Object> entry : register.faker.entrySet()) {
            // 先清除掉要被伪造的已存在的spring bean定义
            String beanName = entry.getKey();
            if (registry.containsBeanDefinition(beanName)) {
                registry.removeBeanDefinition(beanName);
            }
            BeanDefinition definition = StubFactoryBean.definition(entry.getValue());
            registry.registerBeanDefinition(beanName, definition);
        }
        for (Map.Entry<String, Class> entry : register.beans.entrySet()) {
            String beanName = entry.getKey();
            if (registry.containsBeanDefinition(beanName)) {
                registry.removeBeanDefinition(beanName);
            }
            GenericBeanDefinition gbd = new GenericBeanDefinition();
            gbd.setBeanClass(entry.getValue());
            registry.registerBeanDefinition(beanName, gbd);
        }
    }

    /**
     * 替换spring bean为伪造实例
     * {@inheritDoc}
     */
    @Override
    public void postProcessBeanFactory(@NonNull ConfigurableListableBeanFactory beanFactory) throws BeansException {
        for (Map.Entry<String, Object> entry : register.faker.entrySet()) {
            if (!beanFactory.containsBean(entry.getKey())) {
                beanFactory.registerSingleton(entry.getKey(), entry.getValue());
            }
        }
    }
}