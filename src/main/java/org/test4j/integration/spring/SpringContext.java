package org.test4j.integration.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.AliasFor;
import org.springframework.core.io.support.PropertySourceFactory;
import org.springframework.test.context.ContextConfiguration;
import org.test4j.annotations.Named;
import org.test4j.integration.spring.registrar.BeansScanRegistrar;
import org.test4j.module.spring.YamlPropertyFactory;

import java.lang.annotation.*;

/**
 * 整合spring以下配置功能:
 * 1. ContextConfiguration: 指定Configuration类
 * 2. basePackages: 指定扫描路径
 * 3. PropertySource: 配置文件(.yaml, .yml, .properties)加载
 * 4. beans, names: 通过class快速定义spring bean
 * 5. autowired: 指定三方框架使用的依赖注入注解, 比如dubbo的@Reference
 *
 * @author darui.wu
 */
@SuppressWarnings("unused")
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@ContextConfiguration
@ComponentScan
@PropertySource({"empty_nothing.properties"})
@Import(BeansScanRegistrar.class)
public @interface SpringContext {
    /**
     * @see ContextConfiguration#classes()
     */
    @AliasFor(annotation = ContextConfiguration.class, attribute = "classes")
    Class<?>[] classes() default {};

    /**
     * 扫描路径
     */
    @AliasFor(annotation = ComponentScan.class, attribute = "basePackages")
    String[] basePackages() default {};

    @AliasFor(annotation = ComponentScan.class, attribute = "excludeFilters")
    ComponentScan.Filter[] excludeFilters() default {};

    /**
     * @see PropertySource#value()
     */
    @AliasFor(annotation = PropertySource.class, attribute = "value")
    String[] properties() default {"empty_nothing.properties"};

    /**
     * @see PropertySource#factory()
     * <p>
     * 允许 *.properteis 和 *.yaml 格式混合使用
     */
    @AliasFor(annotation = PropertySource.class, attribute = "factory")
    Class<? extends PropertySourceFactory> propertyFactory() default YamlPropertyFactory.class;

    /**
     * <pre>
     * 按 <bean class="class value"/> 方式定义
     * </pre>
     */
    Class[] beans() default {};

    /**
     * <pre>
     * 按 <bean name="names value" class="class value"/> 方式定义
     * </pre>
     */
    Named[] names() default {};

    /**
     * 允许自动注入依赖的注解
     * 比如Dubbo的 @Reference 注解
     */
    Class<? extends Annotation>[] autowired() default {};
}