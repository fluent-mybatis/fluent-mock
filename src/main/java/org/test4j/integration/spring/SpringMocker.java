package org.test4j.integration.spring;

import org.test4j.integration.spring.faker.PlaceholderMocker;

/**
 * 常用mock up工具入口
 *
 * @author darui.wu
 */
@SuppressWarnings("unused")
public interface SpringMocker {
    /**
     * 配置项值获取mock
     *
     * @return PlaceholderMockUp
     */
    static PlaceholderMocker placeholder() {
        return new PlaceholderMocker();
    }
}