package org.test4j.integration.spring;

import org.test4j.integration.spring.faker.FakeAutowiredAnnotationBeanPostProcessor;
import org.test4j.integration.spring.faker.SpringBeanMocker;
import org.test4j.mock.faking.util.ClassLoad;
import org.test4j.mock.startup.IStartupMock;

public class SpringStartup implements IStartupMock {
    public static final String AutowiredAnnotationBeanPostProcessor_Name = "org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor";

    public static final String AbstractBeanFactory = "org.springframework.beans.factory.support.AbstractBeanFactory";

    public static final String PropertyPlaceholderHelper = "org.springframework.util.PropertyPlaceholderHelper";

    @Override
    public void initial() {
        if (ClassLoad.isInClasspath(AbstractBeanFactory)) {
            new SpringBeanMocker();
        }
        if (ClassLoad.isInClasspath(PropertyPlaceholderHelper)) {
            SpringMocker.placeholder();
        }
        if (ClassLoad.isInClasspath(AutowiredAnnotationBeanPostProcessor_Name)) {
            new FakeAutowiredAnnotationBeanPostProcessor();
        }
    }
}