package org.test4j.integration.spring.faker;

import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.test4j.annotations.Mock;
import org.test4j.mock.Invocation;
import org.test4j.mock.MockUp;
import org.test4j.mock.stub.IFakeStub;

import java.beans.PropertyDescriptor;

/**
 * 跳过 {@link org.springframework.beans.factory.annotation.Value}注入
 *
 * @author darui.wu
 */
@SuppressWarnings({"unused"})
public class FakeAutowiredAnnotationBeanPostProcessor extends MockUp<AutowiredAnnotationBeanPostProcessor> {
    /**
     * 如果是Fake Stub, 跳过 {@link org.springframework.beans.factory.annotation.Value}值注入
     */
    @Mock
    public PropertyValues postProcessPropertyValues(
        Invocation inv, PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) {
        if (bean instanceof IFakeStub) {
            return pvs;
        } else {
            return inv.proceed();
        }
    }
}