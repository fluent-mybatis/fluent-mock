package org.test4j.integration.spring.faker;

import org.springframework.util.PropertyPlaceholderHelper;
import org.test4j.annotations.Mock;
import org.test4j.mock.Invocation;
import org.test4j.mock.MockUp;

import java.util.HashMap;
import java.util.Map;

/**
 * 配置项值获取mock
 *
 * @author darui.wu
 */
@SuppressWarnings("unused")
public class PlaceholderMocker extends MockUp<PropertyPlaceholderHelper> {
    private final Map<String, String> values = new HashMap<>();

    @Mock
    public String replacePlaceholders(Invocation it, String value, PropertyPlaceholderHelper.PlaceholderResolver placeholderResolver) {
        if (values.containsKey(value)) {
            return values.get(value);
        }
        try {
            return it.proceed(value, placeholderResolver);
        } catch (IllegalArgumentException e) {
            return "0";
        }
    }

    public PlaceholderMocker putAll(Map<String, String> values) {
        this.values.putAll(values);
        return this;
    }

    public PlaceholderMocker put(String key, String value) {
        this.values.put(key, value);
        return this;
    }
}