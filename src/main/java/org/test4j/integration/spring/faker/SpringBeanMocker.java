package org.test4j.integration.spring.faker;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.test4j.Context;
import org.test4j.annotations.Mock;
import org.test4j.annotations.Mocks;
import org.test4j.integration.spring.SpringContext;
import org.test4j.mock.Invocation;
import org.test4j.mock.MockUp;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/**
 * 注册bean和获取bean拦截处理
 *
 * @author darui.wu
 */
@SuppressWarnings({"unused", "rawtypes"})
public class SpringBeanMocker {
    /**
     * 已处理过的spring容器
     */
    private final Set<Class> exists = new HashSet<>();

    /**
     * 将TestedClass加入spring
     */
    public SpringBeanMocker() {
        new MockUp<DefaultListableBeanFactory>() {
            /**
             * {@link DefaultListableBeanFactory#registerBeanDefinition(String, BeanDefinition)}
             */
            @Mock
            public void registerBeanDefinition(Invocation it, String beanName, BeanDefinition beanDefinition) {
                Class<?> testedClass = Context.currTestClass();
                DefaultListableBeanFactory factory = it.getTarget();
                SpringBeanMocker.this.addTestClassAsBean(factory, testedClass);
                it.proceed(beanName, beanDefinition);
            }
        };
    }

    private void addTestClassAsBean(DefaultListableBeanFactory factory, Class<?> testedClass) {
        if (exists.contains(testedClass)) {
            return;
        }
        exists.add(testedClass);
        while (testedClass != null && testedClass != Object.class) {
            /*
             * @Inherit注解, 不能直接使用 isAnnotationPresent判断
             */
            SpringContext a = testedClass.getDeclaredAnnotation(SpringContext.class);
            if (a != null || testedClass.isAnnotationPresent(Mocks.class)) {
                addBeanRegisterDefinition(factory, testedClass);
            }
            testedClass = testedClass.getSuperclass();
        }
    }

    /**
     * 探测beanRegisterClass是否存在，如果存在，则加入到spring定义中
     *
     * @param factory     spring容器
     * @param testedClass 实现类
     */
    private void addBeanRegisterDefinition(DefaultListableBeanFactory factory, Class testedClass) {
        String beanName = testedClass.getSimpleName();
        if (factory.containsBeanDefinition(beanName)) {
            return;
        }
        if (Modifier.isAbstract(testedClass.getModifiers())) {
            throw new IllegalStateException("The class[" + testedClass.getName() + "] annotated by @SpringContext/@Mocker can't be Abstract!");
        }
        try {
            GenericBeanDefinition gbd = new GenericBeanDefinition();
            gbd.setBeanClass(testedClass);

            factory.registerBeanDefinition(beanName, gbd);
        } catch (Exception e) {
            throw new RuntimeException("add BeanRegister[" + testedClass.getName() + "] bean definition error:" + e.getMessage(), e);
        }
    }
}