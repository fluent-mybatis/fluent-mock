package org.test4j.integration.spring.faker;

import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.lang.NonNull;

import static org.springframework.beans.factory.support.BeanDefinitionBuilder.rootBeanDefinition;

/**
 * 将Stub实例注入到spring容器中
 *
 * @author darui.wu
 */
@SuppressWarnings({"rawtypes"})
public class StubFactoryBean extends AbstractFactoryBean {
    private final Object stub;

    public StubFactoryBean(Object stub) {
        this.stub = stub;
    }

    @Override
    public Class<?> getObjectType() {
        return stub.getClass();
    }

    @NonNull
    @Override
    protected Object createInstance() {
        return stub;
    }

    public static BeanDefinition definition(Object target) {
        BeanDefinitionBuilder builder = rootBeanDefinition(StubFactoryBean.class);
        builder.addConstructorArgValue(target);
        builder.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
        return builder.getBeanDefinition();
    }
}