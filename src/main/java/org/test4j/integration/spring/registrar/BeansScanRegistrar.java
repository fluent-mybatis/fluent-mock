package org.test4j.integration.spring.registrar;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.lang.NonNull;
import org.test4j.integration.spring.SpringContext;

import java.util.Arrays;
import java.util.List;

import static org.springframework.beans.factory.support.BeanDefinitionBuilder.rootBeanDefinition;
import static org.test4j.integration.spring.BeanRegister.camelName;

/**
 * spring mock bean注册处理
 *
 * @author darui.wu
 */
public class BeansScanRegistrar implements ImportBeanDefinitionRegistrar {
    private static final String BeansAutowiredPostProcessorName = "beansAutowiredPostProcessor";

    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, @NonNull BeanDefinitionRegistry registry) {
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(SpringContext.class.getName()));
        assert attributes != null;
        List<Class<?>> autowiredClasses = Arrays.asList(attributes.getClassArray("autowired"));
        this.registryAutowiredPostProcessor(registry, autowiredClasses);
        this.registryBeans(registry, attributes);
    }

    /**
     * 注册bean依赖注入注解, 比如dubbo的@Reference
     *
     * @param registry BeanDefinitionRegistry
     */
    private void registryAutowiredPostProcessor(BeanDefinitionRegistry registry, List<Class<?>> autowiredClasses) {
        if (autowiredClasses.isEmpty() || registry.containsBeanDefinition(BeansAutowiredPostProcessorName)) {
            return;
        }
        BeanDefinitionBuilder builder = rootBeanDefinition(MocksAutowiredPostProcessor.class);
        builder.addConstructorArgValue(autowiredClasses);
        builder.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
        registry.registerBeanDefinition(BeansAutowiredPostProcessorName, builder.getBeanDefinition());
    }

    protected void registryBeans(BeanDefinitionRegistry registry, AnnotationAttributes attributes) {
        Class<?>[] beans = attributes.getClassArray("beans");
        for (Class<?> beanType : beans) {
            String beanName = camelName(beanType);
            this.registryBean(registry, beanName, beanType);
        }
        AnnotationAttributes[] names = attributes.getAnnotationArray("names");
        for (AnnotationAttributes name : names) {
            String beanName = name.getString("name");
            Class<?> beanType = name.getClass("type");
            this.registryBean(registry, beanName, beanType);
        }
    }

    private void registryBean(BeanDefinitionRegistry registry, String beanName, Class<?> beanType) {
        GenericBeanDefinition gbd = new GenericBeanDefinition();
        gbd.setBeanClass(beanType);
        registry.registerBeanDefinition(beanName, gbd);
    }
}