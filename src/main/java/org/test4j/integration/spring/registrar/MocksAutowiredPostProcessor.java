package org.test4j.integration.spring.registrar;

import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.lang.NonNull;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;

public class MocksAutowiredPostProcessor extends AutowiredAnnotationBeanPostProcessor {
    public MocksAutowiredPostProcessor(List<Class<? extends Annotation>> autowiredClasses) {
        this.setAutowiredAnnotationTypes(new HashSet<>(autowiredClasses));
    }

    @Override
    protected boolean determineRequiredStatus(@NonNull MergedAnnotation<?> ann) {
        return false;
    }

    @SuppressWarnings("all")
    @Override
    protected boolean determineRequiredStatus(@NonNull AnnotationAttributes ann) {
        return false;
    }
}