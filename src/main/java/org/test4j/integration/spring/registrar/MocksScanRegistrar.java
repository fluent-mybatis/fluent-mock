package org.test4j.integration.spring.registrar;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.lang.NonNull;
import org.test4j.annotations.Mocks;
import org.test4j.integration.spring.BeanRegister;
import org.test4j.integration.spring.BeanRegisterProcessor;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.beans.factory.support.BeanDefinitionBuilder.rootBeanDefinition;
import static org.test4j.integration.spring.BeanRegister.camelName;

/**
 * spring mock bean注册处理
 *
 * @author darui.wu
 */
@SuppressWarnings({"rawtypes"})
public class MocksScanRegistrar implements ImportBeanDefinitionRegistrar, InitializingBean {

    private static final String MocksBeanRegisterProcessorName = "mocksBeanRegisterProcessor";

    private static final ThreadLocal<BeanRegister> TL_MockRegister = new ThreadLocal<>();

    @Override
    public void registerBeanDefinitions(AnnotationMetadata metadata, @NonNull BeanDefinitionRegistry registry) {
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(Mocks.class.getName()));
        assert attributes != null;

        Map<String, Class> beans = this.getBeans(attributes);
        this.registryBeanRegisterProcessor(registry, beans);
    }

    private Map<String, Class> getBeans(AnnotationAttributes attributes) {
        Map<String, Class> beans = new HashMap<>();
        for (Class<?> beanType : attributes.getClassArray("beans")) {
            String beanName = camelName(beanType);
            beans.put(beanName, beanType);
        }
        for (AnnotationAttributes name : attributes.getAnnotationArray("names")) {
            String beanName = name.getString("name");
            Class<?> beanType = name.getClass("type");
            beans.put(beanName, beanType);
        }
        return beans;
    }

    /**
     * 注册 mock bean 处理Processor
     *
     * @param registry BeanDefinitionRegistry
     */
    private void registryBeanRegisterProcessor(BeanDefinitionRegistry registry, Map<String, Class> beans) {
        if (TL_MockRegister.get() == null) {
            TL_MockRegister.set(new BeanRegister());
        }
        for (Map.Entry<String, Class> entry : beans.entrySet()) {
            TL_MockRegister.get().stub(entry.getKey(), entry.getValue());
        }
        if (beans.isEmpty() || registry.containsBeanDefinition(MocksBeanRegisterProcessorName)) {
            return;
        }
        BeanDefinitionBuilder builder = rootBeanDefinition(BeanRegisterProcessor.class);
        builder.addConstructorArgValue(TL_MockRegister.get());
        builder.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
        registry.registerBeanDefinition(MocksBeanRegisterProcessorName, builder.getBeanDefinition());
    }


    @Override
    public void afterPropertiesSet() {
        TL_MockRegister.set(null);
    }
}