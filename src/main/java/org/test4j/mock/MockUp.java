package org.test4j.mock;

import org.test4j.mock.faking.meta.*;
import org.test4j.mock.functions.Executor;
import org.test4j.mock.startup.Startup;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.test4j.mock.faking.util.ObjectIdentify.identities;

/**
 * 定义mock行为
 *
 * @param <T> 被mock类
 * @see #MockUp(Object...)
 */
@SuppressWarnings({"rawtypes", "unused"})
public class MockUp<T> extends AbstractFake {
    public MockUp(Class declaredClass, Object... objects) {
        super(declaredClass, identities(objects));
        this.initFakeMethods();
    }

    protected MockUp() {
        this(new Object[0]);
    }

    protected MockUp(Object... objects) {
        super(identities(objects));
        this.initFakeMethods();
    }

    protected MockUp(String fullClassName, Object... objects) {
        super(fullClassName, identities(objects));
        this.initFakeMethods();
    }

    protected MockUp(Class declaredClass, Set<Integer> fakedHashCodes) {
        super(declaredClass, fakedHashCodes);
        this.initFakeMethods();
    }

    /**
     * 修改字节码实现
     */
    protected void initFakeMethods() {
        Class fakeClass = this.getClass();
        FakeMethods fakeMethods = new FakeMethods(fakedSeqNo);
        Set<String> matched = new HashSet<>();
        while (MockUp.class.isAssignableFrom(fakeClass) && fakeClass != MockUp.class) {
            List<MethodId> methods = new ClassMeta(declaredToFake, fakeClass).methods;
            for (MethodId methodId : methods) {
                if (matched.contains(methodId.methodDesc)) {
                    continue;
                }
                FakeMethod fakeMethod = new FakeMethod(this, methodId);
                fakeMethods.add(fakeMethod);
                matched.add(methodId.methodDesc);
            }
            fakeClass = fakeClass.getSuperclass();
        }
        FakeStates.register(fakeMethods);
    }

    /**
     * 进行全局化的mock, 从执行global代码开始到所有测试执行完毕
     * mocker的行为会一直有效
     *
     * @param mocker mocker行为
     */
    public synchronized static void global(Executor mocker) {
        Startup.initializing = true;
        try {
            mocker.execute();
        } finally {
            Startup.initializing = false;
        }
    }
}