package org.test4j.mock.faking.fluent;

import org.test4j.mock.Invocation;

/**
 * 自定义mock方法
 *
 * @author darui.wu
 */
@FunctionalInterface
public interface FakeFunction<R> extends MockBehavior {
    /**
     * 定义mock方法实现
     *
     * @param it mock method proxy
     * @return ignore
     */
    R doFake(Invocation it);
}