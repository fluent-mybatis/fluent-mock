package org.test4j.mock.faking.fluent;

import lombok.Getter;

/**
 * mock结果值
 *
 * @author darui.wu
 */
@Getter
public class FakeResult implements MockBehavior {
    /**
     * 结果是否是抛出异常
     */
    private final boolean isThrowable;

    private final Object result;

    private FakeResult(boolean isThrowable, Object result) {
        this.isThrowable = isThrowable;
        this.result = result;
    }

    /**
     * 构建返回值
     *
     * @param value fake return value
     * @return ignore
     */
    public static FakeResult value(Object value) {
        return new FakeResult(false, value);
    }

    /**
     * 构建抛出异常
     *
     * @param e fake throw exception
     * @return ignore
     */
    public static FakeResult eject(Throwable e) {
        return new FakeResult(true, e);
    }
}