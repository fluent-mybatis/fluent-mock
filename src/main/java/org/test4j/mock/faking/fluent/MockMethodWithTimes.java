package org.test4j.mock.faking.fluent;

import org.test4j.mock.faking.meta.TimesVerify;

import static org.test4j.mock.faking.meta.MethodId.buildMethodDesc;

/**
 * 自定义mock行为
 *
 * @author darui.wu
 */
@SuppressWarnings({"UnusedReturnValue", "rawtypes"})
public class MockMethodWithTimes<R> extends MockMethod<R> {
    private final String classMethod;

    public MockMethodWithTimes(FluentMockUp mockUp, String realClass, String name, String desc) {
        super(mockUp, realClass, name, desc);
        this.classMethod = mockUp.getFakeClass(realClass) + "#" + name + desc;
        mockUp.put(buildMethodDesc(name, desc), this);
    }

    /**
     * 调用次数不少于times次
     *
     * @param times min invoke times
     * @return ignore
     */
    public MockMethod<R> min(int times) {
        TimesVerify.min(this.classMethod, times);
        super.mockUp.apply();
        return this;
    }

    /**
     * 调用次数不超过times次
     *
     * @param times just invoke times
     * @return ignore
     */
    public MockMethod<R> max(int times) {
        TimesVerify.max(this.classMethod, times);
        super.mockUp.apply();
        return this;
    }

    /**
     * 方法应该不被调用
     */
    public void never() {
        TimesVerify.never(this.classMethod);
        super.mockUp.apply();
    }

    /**
     * 调用次数在 [min, max]之间
     *
     * @param min min invoke times
     * @param max max invoke times
     * @return ignore
     */
    public MockMethod<R> between(int min, int max) {
        TimesVerify.between(this.classMethod, min, max);
        super.mockUp.apply();
        return this;
    }

    /**
     * 恰好调用times次
     *
     * @param times just invoke times
     * @return ignore
     */
    public MockMethod<R> just(int times) {
        TimesVerify.just(this.classMethod, times);
        super.mockUp.apply();
        return this;
    }
}