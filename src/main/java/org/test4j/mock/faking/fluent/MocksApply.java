package org.test4j.mock.faking.fluent;

import java.util.function.Consumer;

/**
 * 通过 java processor 方式进行mock的实现基类
 *
 * @author darui.wu
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public abstract class MocksApply {
    /**
     * 根据faker和fake修改字节码
     *
     * @param faker mock行为定义
     * @param fake  MockUp实例
     * @param <M>   type
     * @return self
     */
    protected <M extends MocksApply> M apply(Consumer faker, FluentMockUp fake) {
        faker.accept(fake);
        fake.apply();
        return (M) this;
    }

    /**
     * 针对对象target设置mock行为
     *
     * @param faker mock行为定义
     * @param fake  MockUp实例
     * @param <M>   type
     * @return self
     */
    protected <M extends MocksApply> M apply(Object target, Consumer faker, FluentMockUp fake) {
        if (target == null) {
            throw new IllegalArgumentException("the mocked object can't null.");
        }
        faker.accept(fake);
        fake.apply();
        return (M) this;
    }
}