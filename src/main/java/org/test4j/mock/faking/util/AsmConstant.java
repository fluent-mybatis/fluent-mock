package org.test4j.mock.faking.util;

import static g_asm.org.objectweb.asm.ClassReader.EXPAND_FRAMES;
import static g_asm.org.objectweb.asm.ClassWriter.COMPUTE_FRAMES;
import static g_asm.org.objectweb.asm.Opcodes.ASM7;

/**
 * asm过程中用到的常量
 *
 * @author darui.wu
 */
public interface AsmConstant {
    /**
     * see {@link java.lang.reflect.Modifier}
     */
    int SYNTHETIC = 0x00001000;

    String EMPTY_STR = "";

    char PACKAGE_SEPARATOR_CHAR = '.';

    char INNER_CLASS_SEPARATOR_CHAR = '$';

    String Method_$Init = "$init";

    String Method_Init = "<init>";

    String Method_$CL_Init = "$clinit";

    String Method_CL_INIT = "<clinit>";

    int V1_8 = 52;

    int api_code = ASM7;

    int acceptOptions = EXPAND_FRAMES;

    int writeFlags = COMPUTE_FRAMES;
}