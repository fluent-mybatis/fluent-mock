package org.test4j.mock.faking.util;

import java.util.concurrent.locks.ReentrantLock;

import static org.test4j.mock.faking.util.ReflectUtility.doThrow;
import static org.test4j.mock.faking.util.TypeUtility.PRIMITIVE_CLASS;

@SuppressWarnings({"unused", "unchecked", "rawtypes"})
public final class ClassLoad {
    public static final ClassLoader CLASS_LOADER = SearchingClassLoader.combineLoadersOf(null);

    private ClassLoad() {
    }

    public static ClassLoader loadersOf(Class first, Class... types) {
        return SearchingClassLoader.combineLoadersOf(first, types);
    }

    /**
     * 查找class, initialize = false
     *
     * @param className class name
     * @param <T>       type
     * @return ignore
     */
    public static <T> Class<T> loadClass(String className) {
        if (className == null) {
            return null;
        }
        if (PRIMITIVE_CLASS.containsKey(className)) {
            return PRIMITIVE_CLASS.get(className);
        }
        try {
            String clazzName = className.replace('/', '.');
            return (Class<T>) Class.forName(clazzName, false, CLASS_LOADER);
        } catch (LinkageError | ClassNotFoundException e) {
            return doThrow(e);
        }
    }


    /**
     * 类是否在classpath中
     *
     * @param typeName class name
     * @return ignore
     */
    public static boolean isInClasspath(String typeName) {
        try {
            Class clazz = loadClass(typeName);
            return clazz != null;
        } catch (Throwable ignore) {
            return false;
        }
    }

    private static final ReentrantLock LOCK = new ReentrantLock();

    /**
     * 在某些环境下, 需要在这里静态定义 {@link ReentrantLock} 实例
     * 否则BridgeFakeInvocation不生效 :(
     *
     * @return ignore
     */
    public static boolean isLoading() {
        return LOCK.isHeldByCurrentThread();
    }
}