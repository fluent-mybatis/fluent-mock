package org.test4j.mock.faking.util;

import org.test4j.annotations.Mock;
import org.test4j.mock.Invocation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * TypeDesc
 *
 * @author wudarui
 */
@SuppressWarnings({"unused", "rawtypes"})
public enum TypeDesc {
    T_Object(Object.class),
    T_Invocation(Invocation.class),
    T_Mock(Mock.class),
    T_String(String.class),
    T_Boolean(Boolean.class),
    T_Character(Character.class),
    T_Byte(Byte.class),
    T_Short(Short.class),
    T_Void(Void.class),
    T_Integer(Integer.class),
    T_Float(Float.class),
    T_Long(Long.class),
    T_Double(Double.class),
    T_Method(Method.class),
    T_InvocationHandler(InvocationHandler.class);

    TypeDesc(Class clazz) {
        this.PATH = clazz.getName().replace('.', '/');
        this.DESC = "L" + this.PATH + ";";
    }

    public final String PATH;
    public final String DESC;
}