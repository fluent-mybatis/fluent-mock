package org.test4j.mock.functions;

@FunctionalInterface
public interface ESupplier {
    Object get() throws Exception;
}