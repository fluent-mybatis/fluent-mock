package org.test4j.mock.functions;

@FunctionalInterface
public interface Executor {
    void execute();
}