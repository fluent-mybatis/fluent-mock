package org.test4j.mock.processor;

import com.squareup.javapoet.JavaFile;
import org.test4j.mock.faking.util.StackTrace;

import javax.annotation.processing.*;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Set;
import java.util.stream.Stream;

@SuppressWarnings({"unused"})
abstract class BaseProcessor<A extends Annotation> extends AbstractProcessor {
    private static Filer filer;

    private static Messager messager;

    private static Elements elements;

    private boolean generated = false;

    @Override
    public synchronized void init(ProcessingEnvironment env) {
        super.init(env);
        filer = env.getFiler();
        messager = env.getMessager();
        elements = env.getElementUtils();
    }

    protected abstract Class<A> getAnnotationClass();

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
        if (env.processingOver() || generated) {
            return true;
        }
        for (Element element : env.getRootElements()) {
            A annotation = element.getAnnotation(this.getAnnotationClass());
            if (annotation == null || !(element instanceof TypeElement)) {
                continue;
            }
            try {
                this.doProcessor(element, annotation);
            } catch (Throwable e) {
                error("generate mock files error: {}, {}", e.getMessage(), e);
            }
        }
        this.postProcessor();
        this.generated = true;
        return true;
    }

    public static TypeElement getTypeElement(String className) {
        return elements.getTypeElement(className);
    }

    protected abstract void doProcessor(Element element, A annotation);

    protected void postProcessor() {
    }

    public static void writeFiler(JavaFile javaFile) {
        try {
            javaFile.writeTo(filer);
        } catch (IOException ie) {
            error("Failed to generate java file: {}, {}", ie.getMessage(), ie);
        }
    }

    public static void info(String format, Object... args) {
        log(Diagnostic.Kind.NOTE, format, args);
    }

    public static void error(String format, Object... args) {
        log(Diagnostic.Kind.ERROR, format, args);
    }

    public static void log(Diagnostic.Kind kind, String format, Object... args) {
        try {
            String log = format;
            if (args.length > 0) {
                format = format.replaceAll("\\{}", "%s");
                Object[] _args = Stream.of(args)
                    .map(arg -> arg instanceof Throwable ? StackTrace.toString((Throwable) arg) : String.valueOf(arg))
                    .toArray();
                log = String.format(format, _args);
            }

            messager.printMessage(kind, log);
        } catch (Throwable e) {
            messager.printMessage(Diagnostic.Kind.ERROR, "printMessage error:" + StackTrace.toString(e));
        }
    }
}