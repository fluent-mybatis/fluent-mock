package org.test4j.mock.processor;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.ClassName;
import org.test4j.annotations.Mocks;
import org.test4j.mock.processor.filer.MocksFiler;
import org.test4j.mock.processor.filer.attr.BeanAttribute;

import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Map;
import java.util.Set;

@AutoService(Processor.class)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("org.test4j.annotations.Mocks")
public class MocksProcessor extends BaseProcessor<Mocks> {

    private static final String ATTR_VALUE = "value()";
    private static final String ATTR_BEANS = "beans()";
    private static final String ATTR_NAMES = "names()";

    @Override
    protected Class<Mocks> getAnnotationClass() {
        return Mocks.class;
    }

    @Override
    protected void doProcessor(Element element, Mocks mocks) {
        String fullName = ((TypeElement) element).getQualifiedName().toString();
        BeanAttribute attribute = new BeanAttribute(Mocks.class, (TypeElement) element);
        Set<ClassName> stubs = attribute.getClasses(ATTR_VALUE);
        Set<ClassName> beans = attribute.getClasses(ATTR_BEANS);
        Map<String, ClassName> names = attribute.getNames(ATTR_NAMES, "name", "type");
        {
            stubs.addAll(beans);
            stubs.addAll(names.values());
        }
        if (!stubs.isEmpty()) {
            new MocksFiler(fullName, mocks.suffixMocks(), stubs).writeFiler();
        }
    }
}