package org.test4j.mock.processor.filer;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import org.test4j.integration.spring.BeanRegister;
import org.test4j.mock.processor.MocksProcessor;
import org.test4j.mock.processor.filer.attr.ClassNames;

import javax.lang.model.element.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 统一生成Spring Bean配置类
 * TestClassName + 后缀 BeanRegister
 *
 * @author darui.wu
 */
public class BeanRegisterFiler {
    private final ClassName fullName;

    private final String suffix;

    private final List<String> beans;

    private final Map<String, String> names;

    public BeanRegisterFiler(String fullName, String suffix, Collection<String> beans, Map<String, String> names) {
        this.fullName = ClassNames.getClassName(fullName);
        this.suffix = suffix;
        this.beans = new ArrayList<>(beans);
        this.names = names;
    }

    public void writeFiler() {
        TypeSpec.Builder builder = TypeSpec.classBuilder(fullName.simpleName() + suffix)
            .addModifiers(Modifier.PUBLIC)
            .superclass(BeanRegister.class)
            .addMethod(this.m_constructor());

        JavaFile.Builder javaBuilder = JavaFile.builder(fullName.packageName(), builder.build());
        MocksProcessor.writeFiler(javaBuilder.build());
    }

    private MethodSpec m_constructor() {
        MethodSpec.Builder spec = MethodSpec.constructorBuilder()
            .addModifiers(Modifier.PUBLIC);
        for (String stub : this.beans) {
            ClassName className = ClassNames.getClassName(stub);
            spec.addStatement("this.bean($T.class)", className);
        }
        for (Map.Entry<String, String> item : this.names.entrySet()) {
            ClassName className = ClassNames.getClassName(item.getValue());
            spec.addStatement("this.bean($L, $T.class)", item.getKey(), className);
        }
        return spec.build();
    }
}