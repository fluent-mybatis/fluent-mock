package org.test4j.mock.processor.filer.attr;


import com.squareup.javapoet.ClassName;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import java.util.*;

/**
 * 在 Processor中直接获取嵌套的注解, 会报 MirroredTypeException 异常
 *
 * @author darui.wu
 */
@SuppressWarnings({"unused", "unchecked", "rawtypes"})
public class BeanAttribute {
    private final Class aClass;

    private final TypeElement element;

    public BeanAttribute(Class aClass, TypeElement element) {
        this.aClass = aClass;
        this.element = element;
    }

    /**
     * 返回class列表
     *
     * @param attribute 定义Class[]的属性名称
     * @return ignore
     */
    public Set<ClassName> getClasses(String attribute) {
        AnnotationMirror mirror = getMocksMirror(this.element);
        Set<ClassName> values = new HashSet<>();
        if (mirror == null) {
            return values;
        }

        Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues = mirror.getElementValues();
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : elementValues.entrySet()) {
            ExecutableElement method = entry.getKey();
            AnnotationValue value = entry.getValue();
            if (!method.toString().contains(attribute)) {
                continue;
            }
            List<AnnotationValue> list = (List) value.getValue();
            for (AnnotationValue item : list) {
                ClassName className = (ClassName) ClassName.get((TypeMirror) item.getValue());

                // MocksProcessor.error(cn.packageName() + "$" + cn.simpleName() + " / " + cn.simpleNames());
                values.add(className);
            }
            return values;
        }
        return values;
    }

    /**
     * 解析 {@link org.test4j.annotations.Mocks#names()} 属性列表 (name:value) 键值对列表
     *
     * @param attribute 定义 @Named[] 的属性名称
     * @return ignore
     */
    public Map<String, ClassName> getNames(String attribute, String keyName, String valueName) {
        AnnotationMirror mirror = getMocksMirror(this.element);
        Map<String, ClassName> values = new HashMap<>();

        if (mirror == null) {
            return values;
        }
        Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues = mirror.getElementValues();
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : elementValues.entrySet()) {
            ExecutableElement method = entry.getKey();
            AnnotationValue value = entry.getValue();
            if (!method.toString().contains(attribute)) {
                continue;
            }
            List<? extends AnnotationMirror> list = (List) value.getValue();
            for (AnnotationMirror item : list) {
                String name = null;
                ClassName klass = null;
                for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> attr : item.getElementValues().entrySet()) {
                    String attrName = attr.getKey().toString();
                    if ((keyName + "()").equals(attrName)) {
                        name = attr.getValue().toString();
                    } else if ((valueName + "()").equals(attrName)) {
                        klass = (ClassName) ClassName.get((TypeMirror) attr.getValue().getValue());
                    }
                }
                if (name != null && !name.trim().isEmpty() && klass != null) {
                    values.put(name, klass);
                }
            }
            return values;
        }
        return values;
    }

    private AnnotationMirror getMocksMirror(TypeElement element) {
        List<? extends AnnotationMirror> annotations = element.getAnnotationMirrors();
        for (AnnotationMirror annotation : annotations) {
            if (annotation.getAnnotationType().toString().contains(aClass.getSimpleName())) {
                return annotation;
            }
        }
        return null;
    }
}