package org.test4j.mock.processor.filer.attr;

import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;

import java.util.HashMap;
import java.util.Map;

public class ClassNames {
    public static final String MockUp_Suffix = "FMockUp";

    public static final String Mock_Package_Prefix = "mock.";

    private static final Map<String, ClassName> primitiveBoxed = new HashMap<>(16);

    private static final Map<String, TypeName> primitiveArray = new HashMap<>(16);

    static {
        primitiveBoxed.put("void", ClassName.get(Void.class));
        primitiveBoxed.put("boolean", ClassName.get(Boolean.class));
        primitiveBoxed.put("char", ClassName.get(Character.class));
        primitiveBoxed.put("byte", ClassName.get(Byte.class));
        primitiveBoxed.put("short", ClassName.get(Short.class));
        primitiveBoxed.put("int", ClassName.get(Integer.class));
        primitiveBoxed.put("float", ClassName.get(Float.class));
        primitiveBoxed.put("long", ClassName.get(Long.class));
        primitiveBoxed.put("double", ClassName.get(Double.class));
        // 排除void
        // primitives.put("void", ClassName.VOID);
        primitiveArray.put("boolean[]", ArrayTypeName.of(ClassName.BOOLEAN));
        primitiveArray.put("char[]", ArrayTypeName.of(ClassName.CHAR));
        primitiveArray.put("byte[]", ArrayTypeName.of(ClassName.BYTE));
        primitiveArray.put("short[]", ArrayTypeName.of(ClassName.SHORT));
        primitiveArray.put("int[]", ArrayTypeName.of(ClassName.INT));
        primitiveArray.put("float[]", ArrayTypeName.of(ClassName.FLOAT));
        primitiveArray.put("long[]", ArrayTypeName.of(ClassName.LONG));
        primitiveArray.put("double[]", ArrayTypeName.of(ClassName.DOUBLE));
    }

    private ClassNames() {
    }

    public static ClassName getClassName(String fullName) {
        if (primitiveBoxed.containsKey(fullName)) {
            return primitiveBoxed.get(fullName);
        }
        int index = fullName.lastIndexOf('.');
        String packName = index < 0 ? "" : fullName.substring(0, index);
        String className = index < 0 ? fullName : fullName.substring(index + 1);
        return ClassName.get(packName, className);
    }

    /**
     * 方法返回值类型
     * - 对primitive数组特殊处理
     * - 对数组进行处理
     *
     * @param fullName class full name
     * @return ignore
     */
    public static TypeName getReturnType(String fullName) {
        if (primitiveArray.containsKey(fullName)) {
            return primitiveArray.get(fullName);
        } else if (fullName.endsWith("[]")) {
            TypeName typeName = getReturnType(fullName.substring(0, fullName.length() - 2));
            return ArrayTypeName.of(typeName);
        } else {
            return getClassName(fullName);
        }
    }

    public static ClassName mockup(ClassName className) {
        String packName = className.packageName();
        String klasName = String.join("$", className.simpleNames()) + MockUp_Suffix;
        if (packName.startsWith("java.") || packName.startsWith("sun.") || packName.startsWith("javax.")) {
            return ClassName.get(Mock_Package_Prefix + packName, klasName);
        } else {
            return ClassName.get(packName, klasName);
        }
    }
}