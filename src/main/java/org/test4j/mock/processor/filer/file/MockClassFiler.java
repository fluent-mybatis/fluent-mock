package org.test4j.mock.processor.filer.file;

import com.squareup.javapoet.ClassName;
import g_asm.org.objectweb.asm.ClassReader;
import g_asm.org.objectweb.asm.Type;
import org.test4j.mock.faking.util.ClassFile;
import org.test4j.mock.faking.util.TypeUtility;
import org.test4j.mock.processor.filer.attr.MockUpParseMethodVisitor;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.test4j.mock.faking.modifier.FakeTransformer.notMockType;
import static org.test4j.mock.faking.util.AsmConstant.acceptOptions;

@SuppressWarnings({"rawtypes"})
public class MockClassFiler extends MockUpFiler {
    private final Class clazz;

    public MockClassFiler(Class clazz) {
        super(ClassName.get(clazz));
        this.clazz = clazz;
        super.superClass = this.parserSuperClass();
        this.parseMethodIds();
        this.parseAbstractMethodIds();
    }

    protected void parseMethodIds() {
        ClassReader cr = ClassFile.getClassReader(clazz);
        cr.accept(new MockUpParseMethodVisitor(this), acceptOptions);
    }

    private void parseAbstractMethodIds() {
        List<Method> interfaceMethods = TypeUtility.findInterfaceMethod(clazz);
        for (Method method : interfaceMethods) {
            super.addMethodId(method.getModifiers(), method.getName(), Type.getMethodDescriptor(method));
        }
    }

    protected ClassName parserSuperClass() {
        Class superClass = clazz.getSuperclass();
        if (superClass == null || !Modifier.isPublic(superClass.getModifiers()) || notMockType(superClass.getName())) {
            return null;
        } else {
            return ClassName.get(superClass);
        }
    }
}