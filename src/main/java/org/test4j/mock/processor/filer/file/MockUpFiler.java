package org.test4j.mock.processor.filer.file;

import com.squareup.javapoet.*;
import g_asm.org.objectweb.asm.Type;
import lombok.Getter;
import org.test4j.mock.faking.fluent.FluentMockUp;
import org.test4j.mock.faking.fluent.MethodBehaviors;
import org.test4j.mock.faking.fluent.MockMethodWithTimes;
import org.test4j.mock.faking.meta.MethodId;
import org.test4j.mock.faking.util.TypeUtility;
import org.test4j.mock.processor.MocksProcessor;
import org.test4j.mock.processor.filer.attr.ClassNames;

import javax.lang.model.element.Modifier;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.reflect.Modifier.isAbstract;
import static java.lang.reflect.Modifier.isNative;
import static org.test4j.mock.faking.util.AsmConstant.*;

public abstract class MockUpFiler {
    private static final String SUFFIX_SUPER = "$";

    protected ClassName className;
    /**
     * 在本类中声明实现的方法
     */
    protected List<MethodId> declaredMethodIds = new ArrayList<>();
    /**
     * 类或接口中声明的未实现的抽象方法
     */
    protected List<MethodId> abstractMethodIds = new ArrayList<>();

    /**
     * 已经添加到declaredMethodIds + abstractMethodIds集合中的方法
     */
    private final Set<String> existNames = new HashSet<>();
    /**
     * 检测到的同名方法列表
     */
    private final Set<String> sameNames = new HashSet<>();

    private final String classDesc;
    /**
     * simple name: full name
     */
    @Getter
    protected ClassName superClass;

    public MockUpFiler(ClassName className) {
        this.className = className;
        this.classDesc = TypeUtility.classPath(className.reflectionName());
    }

    protected abstract ClassName parserSuperClass();

    protected abstract void parseMethodIds();

    public MethodId addMethodId(int access, String name, String descriptor) {
        if (Objects.equals(Method_CL_INIT, name) || isNative(access)) {
            return null;
        }
        MethodId methodId = new MethodId(this.classDesc, this.superClass == null ? null : this.superClass.toString(), name, descriptor);
        if (declaredMethodIds.contains(methodId) || abstractMethodIds.contains(methodId)) {
            return null;
        }
        if (isAbstract(access)) {
            abstractMethodIds.add(methodId);
        } else {
            declaredMethodIds.add(methodId);
        }
        if (existNames.contains(name)) {
            sameNames.add(name);
        } else {
            existNames.add(name);
        }
        return methodId;
    }

    public void writeFiler() {
        ClassName mockUp = ClassNames.mockup(this.className);
        TypeSpec.Builder builder = TypeSpec
            .classBuilder(mockUp.simpleName())
            .superclass(ClassName.get(FluentMockUp.class))
            .addModifiers(Modifier.PUBLIC);
        this.build(builder);
        JavaFile.Builder javaBuilder = JavaFile.builder(mockUp.packageName(), builder.build());
        MocksProcessor.writeFiler(javaBuilder.build());
    }

    private void build(TypeSpec.Builder builder) {
        builder.addMethod(this.m_constructor1());
        builder.addMethod(this.m_constructor3());
        for (MethodId meta : this.declaredMethodIds) {
            builder.addField(this.f_method(meta, false));
        }
        for (MethodId meta : this.abstractMethodIds) {
            builder.addField(this.f_method(meta, true));
        }
        if (this.superClass != null) {
            builder.addField(this.f_superMock(this.superClass));
        }
    }

    private MethodSpec m_constructor3() {
        return MethodSpec.constructorBuilder()
            .addParameter(Class.class, "declaredClass")
            .addParameter(MethodBehaviors.class, "behaviors")
            .addParameter(ParameterizedTypeName.get(Set.class, Integer.class), "fakedHashCodes")
            .addStatement("super(declaredClass, behaviors, fakedHashCodes)")
            .addModifiers(Modifier.PUBLIC).build();
    }

    private MethodSpec m_constructor1() {
        return MethodSpec.constructorBuilder()
            .addModifiers(Modifier.PUBLIC)
            .addParameter(ArrayTypeName.of(Object.class), "targets")
            .varargs(true)
            .addStatement("super($S, targets)", this.className.reflectionName())
            .build();
    }

    private FieldSpec f_superMock(ClassName clazz) {
        ClassName type = ClassNames.mockup(clazz);
        return FieldSpec.builder(type, "super$", Modifier.PUBLIC, Modifier.FINAL)
            .initializer("new $T(this.declaredToFake, this.behaviors, super.fakedHashCodes)", type)
            .build();
    }

    private FieldSpec f_method(MethodId meta, boolean isAbstract) {
        String methodName = this.buildMethodMockField(meta.name, meta.getParams());
        if (isAbstract) {
            methodName = methodName + SUFFIX_SUPER;
        }
        TypeName rType = ClassNames.getReturnType(meta.getReturnType().getClassName());
        TypeName type = ParameterizedTypeName.get(ClassName.get(MockMethodWithTimes.class), rType);
        FieldSpec.Builder spec = FieldSpec.builder(type, methodName, Modifier.PUBLIC, Modifier.FINAL)
            .initializer("new MockMethodWithTimes<>(this, $S, $S, $S)", isAbstract ? null : meta.realClassDesc, meta.name, meta.desc);
        this.addJavaDoc(spec, methodName, meta);
        return spec.build();
    }

    /**
     * 添加方法注释
     */
    private void addJavaDoc(FieldSpec.Builder spec, String methodName, MethodId meta) {
        spec.addJavadoc("mock {@link $T#$L}", this.className, this.linkJavaDoc(meta))
            .addJavadoc(", example code:\n" +
                "<pre>\n" +
                "mocks.$L.$L.restAnswer(inv -> {", this.className.simpleName(), methodName);
        meta.getParameters().forEach((index, nameType) -> {
            String pType = this.getSimpleName(nameType);
            spec.addJavadoc("\n\t$L $L = inv.arg($L);", pType, nameType.name, index);
        });
        spec.addJavadoc("\n\treturn null;\n" +
            "});\n" +
            "</pre>");
    }

    private String getSimpleName(MethodId.ParaNameType nameType) {
        String pType = Type.getType(nameType.type).getClassName();
        int index = pType.lastIndexOf('.');
        return index >= 0 ? pType.substring(index + 1) : pType;
    }

    private String linkJavaDoc(MethodId meta) {
        StringBuilder buff = new StringBuilder();
        String name = meta.name;
        buff.append(Method_Init.equals(name) ? this.className.simpleName() : name).append("(");
        String paras = Stream.of(meta.getParams())
            .map(Type::getClassName)
            .collect(Collectors.joining(", "));
        buff.append(paras);
        return buff.append(")").toString();
    }

    /**
     * 构造mock方法引导字段名称
     * o 无同名方法，直接使用方法名称
     * o 有同名方法，使用方法名称+参数类型
     *
     * @param name   method name
     * @param params 参数类型列表
     * @return ignore
     */
    private String buildMethodMockField(String name, Type[] params) {
        StringBuilder buff = new StringBuilder();
        if (Objects.equals(Method_Init, name)) {
            buff.append(Method_$Init);
        } else {
            buff.append(name);
        }
        if (!sameNames.contains(name)) {
            return buff.toString();
        }
        for (Type para : params) {
            String clazzName = TypeUtility.getShortClassName(para.getClassName());
            int index = clazzName.indexOf("[]");
            if (index > 0) {
                int size = (clazzName.length() - index) / 2;
                clazzName = clazzName.substring(0, index) + "D" + size;
            }
            buff.append("_").append(clazzName);
        }
        return buff.toString();
    }
}