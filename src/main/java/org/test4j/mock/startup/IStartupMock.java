package org.test4j.mock.startup;

/**
 * 起始阶段mock行为SPI定义
 *
 * @author darui.wu
 */
public interface IStartupMock {
    /**
     * 初始化mock行为
     */
    void initial();
}