package org.test4j.mock.startup;

import java.io.File;
import java.util.regex.Pattern;

/**
 * java-agent使用提示
 *
 * @author wudarui
 */
public class JavaAgentHits {

    private static final Pattern JAR_REGEX = Pattern.compile(".*fluent-mock[-._\\d]*(-SNAPSHOT)?.jar");
    private static final String Dep_Jar_Path = "fluent-mock.jar";

    private static String hits = null;

    private JavaAgentHits() {
    }

    /**
     * 返回 -javaagent:.../fluent-mock.xxx.jar 提示
     *
     * @return ignore
     */
    public static String message() {
        if (hits != null) {
            return hits;
        }
        String jarPath = getJarPath();

        hits = "" +
            "If fluent mock isn't initialized. Please check that your JVM is started with command option:" +
            "\n\n==IDE JVM=====================" +
            "\n\t-javaagent:" + jarPath +
            "\n\n==Maven maven-surefire-plugin=" +
            "\n\t<configuration>" +
            "\n\t\t<argLine>@{argLine} -javaagent:${settings.localRepository}/org/test4j/fluent-mock/${version}/fluent-mock-${version}.jar</argLine>" +
            "\n\t</configuration>" +
            "\n\n==gradle dependencies=========" +
            "\n\tdependencies {" +
            "\n\t\ttest {" +
            "\n\t\t\tjvmArgs \"-javaagent:${classpath.find { it.name.contains(\"fluent-mock\") }.absolutePath}\"" +
            "\n\t\t\tuseJUnitPlatform()" +
            "\n\t\t}" +
            "\n\t}" +
            "\n================================";
        System.err.println(hits);
        return hits;
    }

    private static String getJarPath() {
        String javaClazzPaths = System.getProperty("java.class.path");
        if (javaClazzPaths == null) {
            return Dep_Jar_Path;
        }
        String[] classPath = javaClazzPaths.split(File.pathSeparator);
        for (String cpEntry : classPath) {
            if (JAR_REGEX.matcher(cpEntry).matches()) {
                return cpEntry;
            }
        }
        return Dep_Jar_Path;
    }
}