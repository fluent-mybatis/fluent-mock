package org.test4j.mock.stub;

import g_cglib.net.sf.cglib.proxy.MethodInterceptor;
import g_cglib.net.sf.cglib.proxy.MethodProxy;
import org.test4j.mock.Invocation;
import org.test4j.mock.faking.FakeInvoker;
import org.test4j.mock.faking.meta.FakeMethod;
import org.test4j.mock.faking.meta.FakeStates;
import org.test4j.mock.faking.meta.MethodId;
import org.test4j.mock.faking.meta.TimesVerify;

import java.lang.reflect.Method;

import static java.lang.String.format;

@SuppressWarnings({"rawtypes"})
public class FakeInterceptor implements MethodInterceptor {
    private final Class aClass;

    public FakeInterceptor(Class aClass) {
        this.aClass = aClass;
    }

    @Override
    public Object intercept(Object target, Method method, Object[] args, MethodProxy methodProxy) {
        MethodId methodId = new MethodId(this.aClass, method);
        FakeMethod fakeMethod = FakeStates.getLastMethod(methodId);
        if (fakeMethod == null) {
            throw new AssertionError(format("Method(%s#%s) not implemented or not mocked.",
                method.getDeclaringClass().getSimpleName(), method.getName()));
        } else if (fakeMethod.isProceeding()) {
            throw new RuntimeException(format("the abstract method(%s#%s) can't be reentry again.",
                method.getDeclaringClass().getSimpleName(), method.getName()));
        }
        FakeInvoker invoker = new FakeInvoker(target, fakeMethod, this.aClass, method, args) {
            @Override
            protected Invocation newInvocation() {
                return new StubFakeInvocation(target, args, fakeMethod.getTimesInvoked());
            }
        };
        TimesVerify.increaseTimes(methodId);
        return invoker.callFakeMethod();
    }
}