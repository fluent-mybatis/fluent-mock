package org.test4j.mock.stub;

import org.test4j.mock.Invocation;

@SuppressWarnings({"unchecked"})
public class StubFakeInvocation extends Invocation {
    private final Object target;
    private final Object[] args;
    private final int times;

    StubFakeInvocation(Object target, Object[] args, int times) {
        this.target = target;
        this.args = args;
        this.times = times;
    }

    @Override
    public <T> T proceed(Object... args) {
        throw new RuntimeException("the abstract method can't be reentry again.");
    }

    @Override
    public <T> T getTarget() {
        return (T) target;
    }

    @Override
    public Object[] getArgs() {
        return this.args;
    }

    @Override
    public int getInvokedTimes() {
        return this.times;
    }
}