package org.test4j.module;

import java.lang.reflect.Method;

/**
 * ITest4JListener: Test4J测试生命周期监听
 *
 * @author wudarui
 */
@SuppressWarnings({"rawtypes"})
public interface Test4JListener {
    /**
     * 加载后初始化, 只执行一次
     */
    default boolean init() {
        return true;
    }

    /**
     * 监听器名称
     *
     * @return ignore
     */
    default String name() {
        return this.getClass().getSimpleName();
    }

    /**
     * 加载排序, 0表示无序
     *
     * @return ignore
     */
    default int order() {
        return 0;
    }

    /**
     * 在所有 @BeforeClass执行之前
     *
     * @param testClass Class
     */
    void beforeAll(Class testClass);

    /**
     * 在退出测试类之后
     */
    void afterAll();

    /**
     * 在 测试方法和用户自定义的方法级setup(@BeforeEach @Before @BeforeTest)之前
     *
     * @param target test Object
     */
    void beforeMethod(Object target);

    /**
     * 在 @Before @BeforeEach @BeforeTest 之后, 测试方法之前
     *
     * @param target Object
     * @param method Method
     */
    void beforeExecute(Object target, Method method);

    /**
     * 在 执行测试之后, @After @AfterEach @AfterTest 之前
     *
     * @param target Object
     * @param method Method
     * @param e      Exception
     */
    void afterExecute(Object target, Method method, Throwable e);

    /**
     * 在测试方法和用户自定义的teardown(@After @AfterEach @AfterTest)之后
     */
    void afterMethod();
}