package org.test4j.module.spring;

import org.springframework.beans.factory.BeanFactory;

@SuppressWarnings({"rawtypes", "unused"})
public interface ISpring {
    SpringHelper spring = new SpringHelper();

    class SpringHelper {

        /**
         * 如果spring容器有效，返回名称为beanname的spring bean
         *
         * @param beanName bean Name
         * @return ignore
         */
        public <T> T getBean(String beanName) {
            return SpringEnv.getBeanByName(beanName);
        }

        public <T> T getBean(Class beanType) {
            return SpringEnv.getBeanByType(beanType);
        }

        /**
         * 返回当前spring容器
         *
         * @return ignore
         */
        public BeanFactory getBeanFactory() {
            return SpringInit.getSpringContext().orElse(null);
        }
    }
}