package org.test4j.module.spring;

import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;

import java.io.IOException;
import java.util.List;

/**
 * 加载properties文件
 *
 * @author darui.wu
 */
public class YamlPropertyFactory extends DefaultPropertySourceFactory {
    @Override
    public PropertySource<?> createPropertySource(String name, EncodedResource resource) throws IOException {
        String fileName = resource.getResource().getFilename();
        fileName = fileName == null ? "" : fileName.toLowerCase();
        if (fileName.endsWith(".yaml") || fileName.endsWith(".yml")) {
            return this.loadYaml(resource, fileName);
        } else {
            return super.createPropertySource(name, resource);
        }
    }

    /**
     * 加载yaml配置
     *
     * @param resource EncodedResource
     * @param fileName yaml文件
     * @return PropertySource
     * @throws IOException exception
     */
    private PropertySource<?> loadYaml(EncodedResource resource, String fileName) throws IOException {
        List<PropertySource<?>> sources = new YamlPropertySourceLoader().load(fileName, resource.getResource());
        return sources.get(0);
    }
}