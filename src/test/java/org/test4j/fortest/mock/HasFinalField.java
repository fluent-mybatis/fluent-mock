package org.test4j.fortest.mock;

public class HasFinalField {

    private final String name;

    private String name2;

    private final HasFinalField another;

    public HasFinalField(String name, HasFinalField another) {
        this.name = name;
        this.another = another;
    }

    public HasFinalField(String name) {
        this.name = name;
        this.another = new HasFinalField(name, null);
    }
}