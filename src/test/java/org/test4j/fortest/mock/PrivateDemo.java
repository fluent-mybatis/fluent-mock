package org.test4j.fortest.mock;

import lombok.Data;

@Data
public class PrivateDemo {

    private String tag;

    private PrivateDemo() {
        this.tag = "private";
        System.out.print("====");
    }

    private String changeMsg(String msg) {
        return "private:" + msg;
    }

    public String getMsg(String message) {
        return this.changeMsg(message);
    }

    public static PrivateDemo instance() {
        return new PrivateDemo();
    }
}