package org.test4j.fortest.mock.child;

import org.test4j.mock.faking.modifier.BridgeFakeInvocation;
import org.test4j.mock.faking.util.TypeUtility;

import java.lang.reflect.Method;

import static org.test4j.mock.faking.modifier.BridgeFakeInvocation.Enter_Non_Mock_Block;

public class Child extends Parent {
    @Override
    public String say() throws Throwable {
        Object[] args = new Object[]{
            TypeUtility.classPath(Child.class),
            "say",
            "()Ljava/lang/String;"};
        Object fakeReturn = BridgeFakeInvocation.INSTANCE.invoke(this, (Method) null, args);
        if (String.valueOf(fakeReturn) != Enter_Non_Mock_Block) {
            return (String) fakeReturn;
        } else {
            return "child of " + super.say();
        }
    }
}

class Parent {
    public String say() throws Throwable {
        Object[] args = new Object[]{
            TypeUtility.classPath(Parent.class),
            "say",
            "()Ljava/lang/String;"};
        Object fakeReturn = BridgeFakeInvocation.INSTANCE.invoke(this, (Method) null, args);
        if (String.valueOf(fakeReturn) != Enter_Non_Mock_Block) {
            return (String) fakeReturn;
        } else {
            return "parent";
        }
    }
}