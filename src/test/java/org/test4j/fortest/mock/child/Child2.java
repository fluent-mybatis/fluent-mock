package org.test4j.fortest.mock.child;

public class Child2 extends Parent2 {
    @Override
    public String say() throws Throwable {
        return "child of " + super.say();
    }
}

class Parent2 {
    public String say() throws Throwable {
        return "parent";
    }
}