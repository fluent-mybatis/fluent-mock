package org.test4j.fortest.mock.child;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

public class ParentAndChildAllBeMockTest {
    @DisplayName("对应test2非代码增强，在代码中实现增强行为，方便功能跟踪")
    @Test
    void test() throws Throwable {
        new MockUp<Child>() {
            @Mock
            public String say(Invocation inv) {
                return "mock " + inv.proceed();
            }

            @Override
            protected void applyFake() {
                // 不做代码增强, 测试定义里面模拟了增强
            }
        };
        Assert.assertEquals("mock child of parent", new Child().say());
    }

    @DisplayName("对应test1的代码增强")
    @Test
    void test2() throws Throwable {
        new MockUp<Child2>() {
            @Mock
            public String say(Invocation inv) {
                return "mock " + inv.proceed();
            }
        };
        Child2 child = new Child2();
        Assert.assertEquals("mock child of parent", child.say());
    }
}