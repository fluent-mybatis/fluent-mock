package org.test4j.fortest.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config2 {
    @Bean
    public ServiceB serviceB() {
        return new ServiceB();
    }
}