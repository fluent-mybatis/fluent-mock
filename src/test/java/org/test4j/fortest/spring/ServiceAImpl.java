package org.test4j.fortest.spring;

import org.springframework.stereotype.Service;

@Service("serviceA")
public class ServiceAImpl implements ServiceA {
    @Override
    public String say(String hi) {
        return hi;
    }
}