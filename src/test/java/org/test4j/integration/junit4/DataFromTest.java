package org.test4j.integration.junit4;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;

public class DataFromTest {
    @DataFrom("dataForTest")
    @Test
    public void testDataFrom(int input, int expected) {
        Assert.assertEquals(expected, input);
    }

    public static Iterator<Object[]> dataForTest() {
        return new ArrayList<Object[]>() {
            {
                this.add(new Object[]{1, 1});
                this.add(new Object[]{2, 2});
            }
        }.iterator();
    }
}