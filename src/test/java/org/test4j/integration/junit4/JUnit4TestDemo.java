package org.test4j.integration.junit4;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;

/**
 * 简单junit4测试示例
 */
@SuppressWarnings({"all"})
public class JUnit4TestDemo {
    @Test
    public void demo() {
        int count = 10;
        Assert.assertEquals(10, count);
    }

    @DataFrom("dataForDataFrom")
    @Test
    public void testDataFrom(String actual, String expected) {
        Assert.assertEquals(expected, actual);
    }

    public static Iterator dataForDataFrom() {
        return Arrays.asList(
            new String[]{"string", "string"},
            new String[]{"we", "we"}
        ).iterator();
    }
}