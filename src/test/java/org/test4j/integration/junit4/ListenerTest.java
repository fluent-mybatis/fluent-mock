package org.test4j.integration.junit4;


import org.junit.*;
import org.test4j.integration.ListenerFactory;
import org.test4j.module.Test4JListener;

import java.lang.reflect.Method;

public class ListenerTest {
    static StringBuffer buff = new StringBuffer();

    @BeforeClass
    public static void setup() {
        ListenerFactory.setTestListener(new Test4JListener() {
            @Override
            public void beforeAll(Class testClass) {
                buff.append("listener-beforeAll;");
            }

            @Override
            public void afterAll() {
                buff.append("afterAll-listener;");
                String result = buff.toString();
                System.out.println(buff.toString());
                Assert.assertEquals("beforeAll;" +
                    "beforeAll;" +
                    "   beforeMethod-listener;" +
                    "   before;" +
                    "   before;" +
                    "       beforeExecute;" +
                    "       test;" +
                    "       afterExecute;" +
                    "   after;" +
                    "   after;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "   before;" +
                    "   before;" +
                    "       beforeExecute;" +
                    "       test;" +
                    "       afterExecute;" +
                    "   after;" +
                    "   after;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "   before;" +
                    "   before;" +
                    "       beforeExecute;" +
                    "       test;" +
                    "       afterExecute;" +
                    "   after;" +
                    "   after;" +
                    "   afterMethod-listener;" +
                    "afterAll;" +
                    "afterAll-listener;", result);
                ListenerFactory.setTestListener(null);
            }

            @Override
            public void beforeMethod(Object target) {
                buff.append("   beforeMethod-listener;");
            }

            @Override
            public void afterMethod() {
                buff.append("   afterMethod-listener;");
            }

            @Override
            public void beforeExecute(Object target, Method method) {
                buff.append("       beforeExecute;");
            }

            @Override
            public void afterExecute(Object target, Method method, Throwable e) {
                buff.append("       afterExecute;");
            }
        });
    }

    @BeforeClass
    public static void beforeAll_1() {
        buff.append("beforeAll;");
    }

    @BeforeClass
    public static void beforeAll_2() {
        buff.append("beforeAll;");
    }


    @Before
    public void before_1() {
        buff.append("   before;");
    }

    @Before
    public void before_2() {
        buff.append("   before;");
    }

    @Test
    public void test1() {
        buff.append("       test;");
    }

    @Test
    public void test2() {
        buff.append("       test;");
    }

    @Test
    public void test3() {
        buff.append("       test;");
    }

    @After
    public void after_1() {
        buff.append("   after;");
    }

    @After
    public void after_2() {
        buff.append("   after;");
    }

    @AfterClass
    public static void afterAll_1() {
        buff.append("afterAll;");
    }
}