package org.test4j.integration.junit4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.test4j.annotations.BeforeSpring;

import java.util.HashMap;
import java.util.Map;

@ContextConfiguration(classes = SpringDemoTest.BeanConfig.class)
public class SpringDemoTest {
    @Autowired
    @Qualifier("myMap")
    Map myMap;

    private static int anInt = 100;

    @BeforeSpring
    public static void init() {
        anInt = 200;
    }

    @Test
    public void test() {
        String value = (String) myMap.get("q");
        Assertions.assertEquals("q", value);
        Assertions.assertEquals(200, anInt);
    }

    public static class BeanConfig {
        @Bean
        @Qualifier("myMap")
        public Map myMap() {
            Map map = new HashMap();
            map.put("q", "q");
            return map;
        }
    }
}