package org.test4j.integration.junit4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ContextConfiguration;
import org.test4j.fortest.spring.Config2;
import org.test4j.fortest.spring.ServiceA;
import org.test4j.fortest.spring.SpringConfig;

/**
 * 使用spring容器测试简单示例
 */
@ContextConfiguration(classes = {SpringConfig.class})
public class SpringTestDemo {
    @Autowired
    private ServiceA serviceA;

    @Test
    public void test() {
        String result = this.serviceA.say("welcome to spring test");
        Assertions.assertEquals("welcome to spring test", result);
    }
}