package org.test4j.integration.junit5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 简单junit4测试示例
 */
public class JUnit5TestDemo {
    @Test
    public void demo() {
        int count = 10;
        Assertions.assertEquals(10, count);
    }

    @ParameterizedTest
    @MethodSource("dataForDataFrom")
    void testDataFrom(String actual, String expected) {
        Assertions.assertEquals(expected, actual);
    }

    public static Iterator<Object[]> dataForDataFrom() {
        return new ArrayList<Object[]>() {
            {
                add(new Object[]{"string", "string"});
                add(new Object[]{"we", "we"});
            }
        }.iterator();
    }
}