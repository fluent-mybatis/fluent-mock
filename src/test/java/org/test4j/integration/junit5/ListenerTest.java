package org.test4j.integration.junit5;


import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.test4j.integration.ListenerFactory;
import org.test4j.module.Test4JListener;

import java.lang.reflect.Method;

public class ListenerTest {
    static StringBuffer buff = new StringBuffer();

    static {
        ListenerFactory.setTestListener(new Test4JListener() {
            @Override
            public void beforeAll(Class testClass) {
                buff.append("listener-beforeAll;");
            }

            @Override
            public void afterAll() {
                buff.append("afterAll-listener;");
                String result = buff.toString();
                System.out.println(buff.toString());
                Assert.assertEquals("beforeAll_1;" +
                    "beforeAll_2;" +
                    "   beforeMethod-listener;" +
                    "   before_1;" +
                    "   before_2;" +
                    "       beforeExecute;" +
                    "       test1;" +
                    "       afterExecute;" +
                    "   after_1;" +
                    "   after_2;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "   before_1;" +
                    "   before_2;" +
                    "       beforeExecute;" +
                    "       test2;" +
                    "       afterExecute;" +
                    "   after_1;" +
                    "   after_2;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "   before_1;" +
                    "   before_2;" +
                    "       beforeExecute;" +
                    "       test3;" +
                    "       afterExecute;" +
                    "   after_1;" +
                    "   after_2;" +
                    "   afterMethod-listener;" +
                    "afterAll_1;" +
                    "afterAll-listener;", result);
                ListenerFactory.setTestListener(null);
            }

            @Override
            public void beforeMethod(Object target) {
                buff.append("   beforeMethod-listener;");
            }

            @Override
            public void afterMethod() {
                buff.append("   afterMethod-listener;");
            }

            @Override
            public void beforeExecute(Object target, Method method) {
                buff.append("       beforeExecute;");
            }

            @Override
            public void afterExecute(Object target, Method method, Throwable e) {
                buff.append("       afterExecute;");
            }
        });
    }

    @BeforeAll
    static void beforeAll_1() {
        buff.append("beforeAll_1;");
    }

    @BeforeAll
    static void beforeAll_2() {
        buff.append("beforeAll_2;");
    }


    @BeforeEach
    void before_1() {
        buff.append("   before_1;");
    }

    @BeforeEach
    void before_2() {
        buff.append("   before_2;");
    }

    @Test
    void test1() {
        buff.append("       test1;");
    }

    @Test
    void test2() {
        buff.append("       test2;");
    }

    @Test
    void test3() {
        buff.append("       test3;");
    }

    @AfterEach
    void after_1() {
        buff.append("   after_1;");
    }

    @AfterEach
    void after_2() {
        buff.append("   after_2;");
    }

    @AfterAll
    static void afterAll_1() {
        buff.append("afterAll_1;");
    }
}