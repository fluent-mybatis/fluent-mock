package org.test4j.integration.junit5;


import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.test4j.integration.ListenerFactory;
import org.test4j.module.Test4JListener;

import java.lang.reflect.Method;

public class ListenerTest2 {
    static StringBuffer buff = new StringBuffer();

    static {
        ListenerFactory.setTestListener(new Test4JListener() {
            @Override
            public void beforeAll(Class testClass) {
                buff.append("listener-beforeAll;");
            }

            @Override
            public void afterAll() {
                buff.append("afterAll-listener;");
                String result = buff.toString();
                System.out.println(buff.toString());
                Assert.assertEquals("" +
                    "   beforeMethod-listener;" +
                    "       beforeExecute;" +
                    "       test1;" +
                    "       afterExecute;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "       beforeExecute;" +
                    "       test2;" +
                    "       afterExecute;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "       beforeExecute;" +
                    "       test3;" +
                    "       afterExecute;" +
                    "   afterMethod-listener;" +
                    "afterAll-listener;", result);
                ListenerFactory.setTestListener(null);
            }

            @Override
            public void beforeMethod(Object target) {
                buff.append("   beforeMethod-listener;");
            }

            @Override
            public void afterMethod() {
                buff.append("   afterMethod-listener;");
            }

            @Override
            public void beforeExecute(Object target, Method method) {
                buff.append("       beforeExecute;");
            }

            @Override
            public void afterExecute(Object target, Method method, Throwable e) {
                buff.append("       afterExecute;");
            }
        });
    }

    @Test
    void test1() {
        buff.append("       test1;");
    }

    @Test
    void test2() {
        buff.append("       test2;");
    }

    @Test
    void test3() {
        buff.append("       test3;");
    }
}