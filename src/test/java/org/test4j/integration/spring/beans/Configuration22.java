package org.test4j.integration.spring.beans;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.test4j.integration.spring.BeanRegister;

import java.util.HashMap;

@SuppressWarnings({"rawtypes", "unchecked"})
@Configuration
public class Configuration22 {
    @Bean
    public BeanFactoryPostProcessor beanRegister() {
        return new BeanRegister()
            .bean("myMap", new HashMap() {
                {
                    this.put("A", "abc");
                }
            }).processor();
    }
}