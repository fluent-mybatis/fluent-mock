package org.test4j.integration.spring.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Observer;

@Service
public class ServiceConstruct {
    @Autowired
    public ServiceConstruct(Observer observer) {
        System.out.println("Observer:" + observer.toString());
    }
}