package org.test4j.integration.spring.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.test4j.annotations.Mocks;
import org.test4j.annotations.Named;
import org.test4j.fortest.spring.ServiceA;
import org.test4j.fortest.spring.ServiceAImpl;
import org.test4j.integration.spring.SpringContext;
import org.test4j.integration.spring.beans.Configuration22;
import org.test4j.mock.stubs.FluentStubsTest;

import java.util.HashMap;
import java.util.Map;
import java.util.Observer;

import static org.test4j.integration.spring.demo.BeanRegisterDemoMocks.mocks;

@SuppressWarnings("all")
@Mocks(beans = {
    Observer.class,
    FluentStubsTest.MyNestedService.class
}, names = {@Named(name = "serviceA", type = ServiceA.class)})
@SpringContext(
    classes = {Configuration22.class},
    basePackages = {
        "org.test4j.integration.spring.beans",
        "org.test4j.fortest.spring"
    },
    beans = {ServiceAImpl.class},
    names = @Named(name = "map2", type = HashMap.class)
)
public class BeanRegisterDemo {

    @Autowired
    @Qualifier("serviceA")
    private ServiceA serviceA;

    @Autowired
    @Qualifier("serviceAImpl")
    private ServiceA serviceAImpl;

    @Autowired
    @Qualifier("myMap")
    private Map myMap;

    @Autowired
    private ApplicationContext context;

    @Test
    void assertServiceAIsStub() {
        assert serviceA.getClass().getName().contains("ObjectStub$$EnhancerByCGLIB");
        assert serviceAImpl.getClass().getName().contains(ServiceAImpl.class.getName());
    }

    @Test
    void getByType() {
        try {
            context.getBean(ServiceA.class);
            assert false;
        } catch (NoUniqueBeanDefinitionException e) {
            String err = e.getMessage();
            assert err.contains("but found 2: serviceAImpl,serviceA");
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    void test() {
        mocks.ServiceA(fake -> {
            fake.say$.restReturn("mock value");
        });
        String result = serviceA.say("h");
        Assertions.assertEquals("mock value", result);

        Assertions.assertEquals(myMap.get("A"), "abc");
    }
}