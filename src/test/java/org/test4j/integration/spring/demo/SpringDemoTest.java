package org.test4j.integration.spring.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.test4j.annotations.BeforeSpring;
import org.test4j.integration.spring.SpringContext;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"rawtypes"})
@SpringContext(classes = BeanConfig.class, basePackages = "org.test4j.integration.spring.demo")
public class SpringDemoTest {
    @Autowired
    @Qualifier("myMap2")
    Map myMap;

    private static int anInt = 100;

    @BeforeSpring
    public static void init() {
        anInt = 200;
    }

    @Test
    public void test() {
        String value = (String) myMap.get("q");
        Assertions.assertEquals("q", value);
        Assertions.assertEquals(200, anInt);
    }
}

@SuppressWarnings({"rawtypes", "unchecked"})
@Configuration
class BeanConfig {
    @Bean("myMap2")
    public Map myMap() {
        Map map = new HashMap();
        map.put("q", "q");
        return map;
    }
}