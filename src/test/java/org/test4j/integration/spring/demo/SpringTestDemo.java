package org.test4j.integration.spring.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.test4j.fortest.spring.Config2;
import org.test4j.fortest.spring.ServiceA;
import org.test4j.fortest.spring.ServiceB;
import org.test4j.fortest.spring.SpringConfig;
import org.test4j.integration.spring.SpringContext;
import org.test4j.module.spring.ISpring;

/**
 * 使用spring容器测试简单示例
 */
@SpringContext(classes = {SpringConfig.class}, basePackages = "org.test4j.fortest.spring",
    excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = Config2.class)
    })
public class SpringTestDemo implements ISpring {
    @Autowired
    @Qualifier("serviceA")
    private ServiceA serviceA;

    @Test
    public void test() {
        String result = this.serviceA.say("welcome to spring test");
        Assertions.assertEquals("welcome to spring test", result);
    }

    @Test
    public void excludeBean() {
        ServiceA a = spring.getBean(ServiceA.class);
        Assertions.assertNotNull(a);

        ServiceB b = spring.getBean(ServiceB.class);
        Assertions.assertNull(b);
    }
}