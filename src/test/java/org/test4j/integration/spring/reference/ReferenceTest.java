package org.test4j.integration.spring.reference;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.test4j.annotations.Mocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.test4j.integration.spring.reference.ReferenceTestMocks.mocks;


@Mocks(beans = {TestService2.class})
public class ReferenceTest extends BaseTest1 {
    @Autowired
    private TestService service;

    @Test
    void reference() {
        assertNotNull(service);
        mocks.TestService2.sayReference$.thenReturn("autowired mock bean");
        String result = service.service2();
        assertEquals("autowired mock bean", result);
    }
}