package org.test4j.integration.spring.reference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.test4j.annotations.Mocks;

public interface TestService2 {
    String sayReference();
}

class TestService {
    @Reference
    TestService2 service2;

    @Autowired
    private TestService3 service3;

    public String service2() {
        return service2.sayReference();
    }

    public String service3() {
        return service3.sayReference();
    }

}

@Configuration
@Mocks(beans = TestService3.class)
class BeanConfig {
    @Autowired
    private TestService2 service2;

    @Bean
    public TestService testService() {
        return new TestService();
    }
}