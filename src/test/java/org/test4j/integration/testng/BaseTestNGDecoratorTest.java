package org.test4j.integration.testng;

import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;
import org.testng.IHookCallBack;
import org.testng.IHookable;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.applet.Applet;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class BaseTestNGDecoratorTest implements IHookable {
    // Makes sure TestNG integration works with test classes which implement IHookable.
    @Override
    public void run(IHookCallBack callBack, ITestResult testResult) {
        callBack.runTestMethod(testResult);
    }

    public static class FakeClass1 extends MockUp<Applet> {
        @Mock
        public String getAppletInfo() {
            return "TEST1";
        }
    }

    @BeforeMethod
    public final void beforeBase() {
        assertNull(new Applet().getAppletInfo());
        new FakeClass1();
        assertEquals(new Applet().getAppletInfo(), "TEST1");
    }

    @AfterMethod
    public final void afterBase() {
        assertEquals(new Applet().getAppletInfo(), "TEST1");
    }
}