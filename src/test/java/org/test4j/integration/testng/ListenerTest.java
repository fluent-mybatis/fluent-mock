package org.test4j.integration.testng;


import org.test4j.integration.ListenerFactory;
import org.test4j.module.Test4JListener;
import org.testng.Assert;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class ListenerTest {
    static StringBuffer buff = new StringBuffer();

    @BeforeClass
    public static void setup() {
        ListenerFactory.setTestListener(new Test4JListener() {
            @Override
            public void beforeAll(Class testClass) {
                buff.append("listener-beforeAll;");
            }

            @Override
            public void afterAll() {
                buff.append("afterAll-listener;");
                ListenerFactory.setTestListener(null);
                String result = buff.toString();
                Assert.assertEquals(result, "beforeAll_1;" +
                    "beforeAll_2;" +
                    "   beforeMethod-listener;" +
                    "   before_1;" +
                    "   before_2;" +
                    "       beforeExecute;" +
                    "       test1;" +
                    "       afterExecute;" +
                    "   after_1;" +
                    "   after_2;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "   before_1;" +
                    "   before_2;" +
                    "       beforeExecute;" +
                    "       test2;" +
                    "       afterExecute;" +
                    "   after_1;" +
                    "   after_2;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "   before_1;" +
                    "   before_2;" +
                    "       beforeExecute;" +
                    "       test3;" +
                    "       afterExecute;" +
                    "   after_1;" +
                    "   after_2;" +
                    "   afterMethod-listener;" +
                    "afterAll_1;" +
                    "afterAll-listener;");
            }

            @Override
            public void beforeMethod(Object target) {
                buff.append("   beforeMethod-listener;");
            }

            @Override
            public void afterMethod() {
                buff.append("   afterMethod-listener;");
            }

            @Override
            public void beforeExecute(Object target, Method method) {
                buff.append("       beforeExecute;");
            }

            @Override
            public void afterExecute(Object target, Method method, Throwable e) {
                buff.append("       afterExecute;");
            }
        });
    }

    @BeforeClass
    public static void beforeAll_1() {
        buff.append("beforeAll_1;");
    }

    @BeforeClass
    public static void beforeAll_2() {
        buff.append("beforeAll_2;");
    }


    @BeforeMethod
    public void before_1() {
        buff.append("   before_1;");
    }

    @BeforeMethod
    public void before_2() {
        buff.append("   before_2;");
    }

    @Test
    public void test1() {
        buff.append("       test1;");
    }

    @Test
    public void test2() {
        buff.append("       test2;");
    }

    @Test
    public void test3() {
        buff.append("       test3;");
    }

    @AfterMethod
    public void after_1() {
        buff.append("   after_1;");
    }

    @AfterMethod
    public void after_2() {
        buff.append("   after_2;");
    }

    @AfterClass
    public static void afterAll_1() {
        buff.append("afterAll_1;");
    }
}