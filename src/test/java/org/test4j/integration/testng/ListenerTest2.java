package org.test4j.integration.testng;

import org.test4j.integration.ListenerFactory;
import org.test4j.module.Test4JListener;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class ListenerTest2 {
    static StringBuffer buff = new StringBuffer();

    @BeforeClass
    public static void setup() {
        ListenerFactory.setTestListener(new Test4JListener() {
            @Override
            public void beforeAll(Class testClass) {
                buff.append("");
            }

            @Override
            public void afterAll() {
                buff.append("afterAll-listener;");
                String result = buff.toString();
                ListenerFactory.setTestListener(null);
                Assert.assertEquals(result, "" +
                    "   beforeMethod-listener;" +
                    "       beforeExecute;" +
                    "       test1;" +
                    "       afterExecute;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "       beforeExecute;" +
                    "       test2;" +
                    "       afterExecute;" +
                    "   afterMethod-listener;" +
                    "   beforeMethod-listener;" +
                    "       beforeExecute;" +
                    "       test3;" +
                    "       afterExecute;" +
                    "   afterMethod-listener;" +
                    "afterAll-listener;");
            }

            @Override
            public void beforeMethod(Object target) {
                buff.append("   beforeMethod-listener;");
            }

            @Override
            public void afterMethod() {
                buff.append("   afterMethod-listener;");
            }

            @Override
            public void beforeExecute(Object target, Method method) {
                buff.append("       beforeExecute;");
            }

            @Override
            public void afterExecute(Object target, Method method, Throwable e) {
                buff.append("       afterExecute;");
            }
        });
    }

    @Test
    public void test1() {
        buff.append("       test1;");
    }

    @Test
    public void test2() {
        buff.append("       test2;");
    }

    @Test
    public void test3() {
        buff.append("       test3;");
    }
}