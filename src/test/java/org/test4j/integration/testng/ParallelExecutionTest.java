package org.test4j.integration.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.testng.Assert.assertEquals;

public final class ParallelExecutionTest {
    final AtomicInteger counter = new AtomicInteger();

    @Test(threadPoolSize = 4, invocationCount = 10)
    public void parallelExecution() {
        counter.incrementAndGet();
    }

    @AfterClass
    public void checkCounter() {
        assertEquals(counter.get(), 10);
    }
}