package org.test4j.integration.testng;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

@ContextConfiguration(classes = SpringDemoTest.BeanConfig.class)
public class SpringDemoTest extends AbstractTestNGSpringContextTests {
    @Autowired
    @Qualifier("myMap")
    Map myMap;

    @Test
    public void test() {
        String value = (String) myMap.get("q");
        Assert.assertEquals("q", value);
    }

    public static class BeanConfig {
        @Bean
        @Qualifier("myMap")
        public Map myMap() {
            Map map = new HashMap();
            map.put("q", "q");
            return map;
        }
    }
}