package org.test4j.integration.testng;

import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;
import org.testng.annotations.*;

import java.applet.Applet;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public final class TestNGFakingTest {
    @BeforeSuite
    void setUpTestSuite() {
        assertNull(MySystem.getenv("BeforeSuite"));
    }

    @BeforeTest
    void setUpTest() {
        assertNull(MySystem.getenv("BeforeTest"));
    }

    @BeforeClass
    void setUpTestClass() {
        new MockUp<MySystem>() {
            @Mock
            String getenv(String name) {
                return name;
            }
        };
    }

    @BeforeMethod
    void setUpTestMethod() {
        assertEquals(MySystem.getenv("BeforeMethod"), "BeforeMethod");

        new MockUp<Applet>() {
            @Mock
            String getAppletInfo() {
                return "MOCK";
            }
        };
    }

    @Test
    public void testSomething() {
        assertEquals(MySystem.getenv("testMethod"), "testMethod");
        assertEquals(new Applet().getAppletInfo(), "MOCK");
    }

    @AfterMethod
    void tearDownTestMethod() {
        assertEquals(MySystem.getenv("AfterMethod"), "AfterMethod");
        assertEquals(new Applet().getAppletInfo(), "MOCK");
    }

    @AfterClass
    void tearDownClass() {
        assertEquals(MySystem.getenv("AfterClass"), "AfterClass");
        assertNull(new Applet().getAppletInfo());
    }

    @AfterTest
    void tearDownTest() {
        assertNull(MySystem.getenv("AfterTest"), "Fake still in effect");
        assertNull(new Applet().getAppletInfo());
    }

    @AfterSuite
    void tearDownSuite() {
        assertNull(MySystem.getenv("AfterSuite"));
        assertNull(new Applet().getAppletInfo());
    }

    static class MySystem {
        public static String getenv(String name) {
            return null;
        }
    }
}