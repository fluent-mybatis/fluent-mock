package org.test4j.mock;

import lombok.Getter;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


class StubsTest {

    @Test
    void stubNullFields() {
        SubObject target = new SubObject();
        assertNull(target.getMap());
        assertNull(target.getAObject());
        assertNull(target.getBObject());
        Stubs.nilFields(target);
        assertNull(target.getMap());
        assertNotNull(target.getAObject());
        assertNotNull(target.getBObject());
        assertNull(target.getBObject().getAObject());
    }

    @Test
    void stubNullFields_recursive() {
        SubObject target = new SubObject();
        assertNull(target.getMap());
        assertNull(target.getAObject());
        assertNull(target.getBObject());
        Stubs.nilFields(target, true);
        assertNull(target.getMap());
        assertNotNull(target.getAObject());
        assertNotNull(target.getBObject());
        assertNotNull(target.getBObject().getAObject());
    }

    @Getter
    static class PObject {
        private Map map;

        private AObject aObject;
    }

    @Getter
    static class SubObject extends PObject {
        private BObject bObject;
    }

    static class AObject {
    }

    @Getter
    static class BObject {
        private AObject aObject;
    }
}