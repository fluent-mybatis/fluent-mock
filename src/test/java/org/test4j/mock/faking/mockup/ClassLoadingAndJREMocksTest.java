package org.test4j.mock.faking.mockup;

import org.junit.jupiter.api.Test;
import org.test4j.Context;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import java.io.File;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class ClassLoadingAndJREMocksTest {
    static class Foo {
    }

    @Test
    public void fakeFile() {
        new MockUp<File>() {
            @Mock
            void $init(String name) {
            }

            @Mock
            boolean exists() {
                return true;
            }
        };

        new Foo(); // causes a class load
        assertTrue(new File("filePath").exists());
        Context.clearNoMockingZone();
    }

    @Test
    public void fakeFileSafelyUsingReentrantFakeMethod() {
        new MockUp<File>() {
            @Mock
            boolean exists(Invocation inv) {
                File it = inv.getTarget();
                return "testFile".equals(it.getName()) || inv.<Boolean>proceed();
            }
        };

        checkForTheExistenceOfSeveralFiles();
    }

    void checkForTheExistenceOfSeveralFiles() {
        assertFalse(new File("someOtherFile").exists());
        assertTrue(new File("testFile").exists());
        assertFalse(new File("yet/another/file").exists());
        assertTrue(new File("testFile").exists());
    }

    @Test
    public void fakeFileSafelyUsingProceed() {
        new MockUp<File>() {
            @Mock
            boolean exists(Invocation inv) {
                File it = inv.getTarget();
                return "testFile".equals(it.getName()) || inv.<Boolean>proceed();
            }
        };

        checkForTheExistenceOfSeveralFiles();
    }
}