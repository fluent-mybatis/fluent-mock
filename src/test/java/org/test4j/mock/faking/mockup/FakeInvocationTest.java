package org.test4j.mock.faking.mockup;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import static org.junit.Assert.*;

public final class FakeInvocationTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    public static class Collaborator {
        int value;

        Collaborator() {
        }

        public Collaborator(int i) {
            value = i;
        }

        public int getValue() {
            return -1;
        }

        public void setValue(int i) {
            value = i;
        }

        public String doSomething(boolean b, int[] i, String s) {
            return s + b + i[0];
        }

        public static boolean staticMethod() {
            return true;
        }
    }

    static final class FakeMethods extends MockUp<Collaborator> {
        @Mock
        static boolean staticMethod(Invocation context) {
            assertNotNull(context);
            assertNull(context.getTarget());
            assertEquals(1, context.getInvokedTimes());
            return false;
        }

        @Mock
        int getValue(Invocation context) {
            assertTrue(context.getTarget() instanceof Collaborator);
            assertEquals(1, context.getInvokedTimes());
            return 123;
        }
    }

    @Test
    public void fakeMethodsForMethodsWithoutParameters() {
        new FakeMethods();
        assertFalse(Collaborator.staticMethod());
        assertEquals(123, new Collaborator().getValue());
    }

    @Test
    public void instanceFakeMethodForStaticMethod() {
        new MockUp<Collaborator>() {
            @Mock
            boolean staticMethod(Invocation context) {
                assertNull(context.getTarget());
                return context.getInvokedTimes() <= 0;
            }
        };

        assertFalse(Collaborator.staticMethod());
        assertFalse(Collaborator.staticMethod());
    }

    @Test
    public void fakeMethodsWithInvocationParameter() {
        new MockUp<Collaborator>() {
            Collaborator instantiated;

            @Mock
            void $init(Invocation inv, int i) {
                assertNotNull(inv.getTarget());
                assertTrue(i > 0);
                instantiated = inv.getTarget();
            }

            @Mock
            String doSomething(Invocation inv, boolean b, int[] array, String s) {
                assertNotNull(inv);
                assertSame(instantiated, inv.getTarget());
                assertEquals(1, inv.getInvokedTimes());
                assertTrue(b);
                assertNull(array);
                assertEquals("test", s);
                return "mock";
            }
        };

        String s = new Collaborator(123).doSomething(true, null, "test");
        assertEquals("mock", s);
    }

    static class FakeMethodsWithParameters extends MockUp<Collaborator> {
        int capturedArgument;
        Collaborator fakedInstance;

        @Mock
        void $init(Invocation context, int i) {
            capturedArgument = i + context.getInvokedTimes();
            assertNull(fakedInstance);
            assertTrue(context.getTarget() instanceof Collaborator);
            assertEquals(1, context.getArgs().length);
        }

        @Mock
        void setValue(Invocation context, int i) {
            assertEquals(i, context.getInvokedTimes() - 1);
            assertSame(fakedInstance, context.getTarget());
            assertEquals(1, context.getArgs().length);
        }
    }

    @Test
    public void fakeMethodsWithParameters() {
        FakeMethodsWithParameters mock = new FakeMethodsWithParameters();

        Collaborator col = new Collaborator(4);
        mock.fakedInstance = col;

        assertEquals(5, mock.capturedArgument);
        col.setValue(0);
        col.setValue(1);
    }

    public static final class PublicFakeForConstructorUsingInvocation extends MockUp<Collaborator> {
        @Mock
        public void $init(Invocation inv) {
        }
    }

    @Test
    public void fakeConstructorUsingPublicFakeClassWithPublicFakeMethodHavingInvocationParameter() {
        new PublicFakeForConstructorUsingInvocation();

        new Collaborator();
    }
}