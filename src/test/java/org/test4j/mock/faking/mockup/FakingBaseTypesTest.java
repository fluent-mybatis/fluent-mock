package org.test4j.mock.faking.mockup;

import org.junit.*;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public final class FakingBaseTypesTest {
    static class BeforeClassBaseAction {
        protected int perform() {
            return -1;
        }
    }

    static class BeforeClassAction extends BeforeClassBaseAction {
        @Override
        protected int perform() {
            return 12;
        }
    }

    @BeforeClass
    public static <T extends BeforeClassBaseAction> void applyFakeForAllTests() {
        new MockUp<T>() {
            @Mock
            int perform() {
                return 34;
            }
        };
    }

    @AfterClass
    public static void verifyFakeForAllTestsIsInEffect() {
        int i1 = new BeforeClassAction().perform();
        int i2 = new BeforeClassBaseAction().perform();

        assertEquals(34, i1);
        assertEquals(34, i2);
    }

    public interface IBeforeAction {
        @SuppressWarnings("unused")
        int perform();
    }

    static class BeforeAction implements IBeforeAction {
        @Override
        public int perform() {
            return 123;
        }
    }

    @Before
    public <T extends IBeforeAction> void applyFakeForEachTest() {
        new MockUp<T>() {
            @Mock
            int perform() {
                return 56;
            }
        };
    }

    @After
    public void verifyFakeForEachTestIsInEffect() {
        int i = new BeforeAction().perform();
        assertEquals(56, i);
    }

    public interface IAction {
        default int perform(int i) {
            return 100;
        }

        boolean notToBeFaked();
    }

    public static class ActionImpl1 implements IAction {

        @Override
        public boolean notToBeFaked() {
            return true;
        }
    }

    static final class ActionImpl2 implements IAction {
        @Override
        public int perform(int i) {
            return i - 2;
        }

        @Override
        public boolean notToBeFaked() {
            return true;
        }
    }

    IAction actionI;

    public abstract static class BaseAction {
        protected abstract int perform(int i);

        public int toBeFakedAsWell() {
            return -1;
        }

        int notToBeFaked() {
            return 1;
        }
    }

    static final class ConcreteAction1 extends BaseAction {
        @Override
        public int perform(int i) {
            return i - 1;
        }
    }

    static class ConcreteAction2 extends BaseAction {
        @Override
        protected int perform(int i) {
            return i - 2;
        }

        @Override
        public int toBeFakedAsWell() {
            return super.toBeFakedAsWell() - 1;
        }

        @Override
        int notToBeFaked() {
            return super.notToBeFaked() + 1;
        }
    }

    static class ConcreteAction3 extends ConcreteAction2 {
        @Override
        public int perform(int i) {
            return i - 3;
        }

        @Override
        public int toBeFakedAsWell() {
            return -3;
        }

        @Override
        final int notToBeFaked() {
            return 3;
        }
    }

    BaseAction actionB;

    @After
    public void checkImplementationClassesAreNoLongerFaked() {
        if (actionI != null) {
            assertEquals(-1, actionI.perform(0));
        }

        assertEquals(-2, new ActionImpl2().perform(0));

        if (actionB != null) {
            assertEquals(-1, actionB.perform(0));
        }

        assertEquals(-2, new ConcreteAction2().perform(0));
        assertEquals(-3, new ConcreteAction3().perform(0));
    }

    static final class FakeInterface extends MockUp<IAction> {
        @Mock
        int perform(int i) {
            return i + 2;
        }
    }

    @Test
    public void test5_fakeInterfaceImplementationClassesUsingNamedFake() {
        new FakeInterface();

        actionI = new ActionImpl1();
        assertEquals(3, actionI.perform(1));
        assertEquals(2, new ActionImpl2().perform(4));
    }

    static final class FakeBaseClass extends MockUp<BaseAction> {
        @Mock
        int perform(int i) {
            return i + 3;
        }
    }

    @Test
    public void test6_fakeConcreteSubclassesUsingNamedFake() {
        new FakeBaseClass();

        actionB = new ConcreteAction1();
        assertEquals(1, actionB.perform(2));
        assertEquals(3, new ConcreteAction2().perform(5));
        assertEquals(2, new ConcreteAction3().perform(5));
    }
}