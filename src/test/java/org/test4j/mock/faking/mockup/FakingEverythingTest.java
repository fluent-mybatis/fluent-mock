package org.test4j.mock.faking.mockup;

import org.test4j.mock.Invocation;
import org.test4j.mock.faking.meta.FakeInvocation;

import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class FakingEverythingTest {
    final List<String> traces = new ArrayList<>();

    void traceEntry(Invocation inv) {
        traces.add("Entered " + getDescription(inv));
    }

    void traceExit(Invocation inv) {
        traces.add("Exited " + getDescription(inv));
    }

    String getDescription(Invocation inv) {
        Member member = ((FakeInvocation) inv).getInvokedMember();
        String args = Arrays.toString(inv.getArgs());
        Object instance = inv.getTarget();
        return member.getDeclaringClass().getSimpleName() + '#' + member.getName() + " with " + args + " on " + instance;
    }
}