package org.test4j.mock.faking.mockup;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import java.io.File;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FileExistReentrantTest {
    @Disabled
    @Test
    public void fakeFileAndForceJREToCallReentrantFakedMethod() {
        assertFalse(new File("noFile").exists());
        new MockUp<File>() {
            @Mock
            boolean exists(Invocation inv) {
                boolean exists = inv.proceed();
                return !exists;
            }
        };
        // Cause the JVM/JRE to load a new class, calling the faked File#exists() method in the process:
        new Runnable() {
            @Override
            public void run() {
            }
        };
        assertTrue(new File("noFile").exists());
    }

    @Test
    public void fakeAndCallReentrantFakedMethod() {
        assertFalse(new Exists("noFile").exists());

        new MockUp<Exists>() {
            @Mock
            boolean exists(Invocation inv) {
                boolean exists = inv.proceed();
                return !exists;
            }
        };

        assertTrue(new Exists("noFile").exists());
    }

    static class Exists {
        private String str;

        public Exists(String str) {
            this.str = str;
        }

        public boolean exists() {
            if (str == null || str.contains("no")) {
                return false;
            } else {
                return true;
            }
        }
    }
}
