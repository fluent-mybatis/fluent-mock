package org.test4j.mock.faking.mockup;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.test4j.fortest.mock.HasFinalField;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

public class HasFinalFieldTest {
    @Test
    void test() {
        int[] is = {0};
        new MockUp<HasFinalField>() {
            @Mock
            public void $init(String name) {
                is[0] = 1;
            }
        };
        new HasFinalField("test");
        Assert.assertEquals(1, is[0]);
    }
}