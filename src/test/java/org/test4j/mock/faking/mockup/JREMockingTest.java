package org.test4j.mock.faking.mockup;

import org.junit.jupiter.api.Test;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import static org.junit.Assert.*;

public final class JREMockingTest {
    @Test
    public void fakingOfCalendar() {
        final Calendar calCST = new GregorianCalendar(2010, Calendar.MAY, 15);
        final TimeZone tzCST = TimeZone.getTimeZone("CST");
        new MockUp<Calendar>() {
            @Mock
            Calendar getInstance(Invocation inv, TimeZone tz) {
                return tz == tzCST ? calCST : inv.<Calendar>proceed();
            }
        };

        Calendar cal1 = Calendar.getInstance(tzCST);
        assertSame(calCST, cal1);
        assertEquals(2010, cal1.get(Calendar.YEAR));

        TimeZone tzPST = TimeZone.getTimeZone("PST");
        Calendar cal2 = Calendar.getInstance(tzPST);
        assertNotSame(calCST, cal2);
    }
}