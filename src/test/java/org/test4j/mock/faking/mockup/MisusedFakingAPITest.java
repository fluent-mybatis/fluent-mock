package org.test4j.mock.faking.mockup;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.test4j.fortest.mock.PrivateDemo;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import java.applet.Applet;

import static org.junit.Assert.assertEquals;

public final class MisusedFakingAPITest {

    @Test
    public void fakeSameMethodTwiceWithReentrantFakesFromTwoDifferentFakeClasses() {
        new MockUp<Applet>() {
            @Mock
            int getComponentCount(Invocation inv) {
                int i = inv.proceed();
                return i + 1;
            }
        };

        int i = new Applet().getComponentCount();
        assertEquals(1, i);

        new MockUp<Applet>() {
            @Mock
            int getComponentCount(Invocation inv) {
                int j = inv.proceed();
                return j + 2;
            }
        };

        // Should return 3, but returns 5. Chaining mock methods is not supported.
        int j = new Applet().getComponentCount();
        assertEquals(2, j);
    }

    static final class AppletFake extends MockUp<Applet> {
        final int componentCount;

        AppletFake(int componentCount) {
            this.componentCount = componentCount;
        }

        @Mock
        int getComponentCount(Invocation inv) {
            return componentCount;
        }
    }

    @Test
    public void applyTheSameFakeForAClassTwice() {
        new AppletFake(1);
        new AppletFake(2); // second application overrides the previous one

        assertEquals(2, new Applet().getComponentCount());
    }

    @Test
    public void fakeAPrivateMethod() {
        new MockUp<PrivateDemo>() {
            @Mock
            String changeMsg(String msg) {
                return "mock:" + msg;
            }
        };
        String msg = PrivateDemo.instance().getMsg("test");
        Assert.assertEquals(msg, "mock:test");
    }

    @Test
    public void fakeAPrivateConstructor() {
        new MockUp<PrivateDemo>() {
            @Mock
            void $init(Invocation inv) {
                PrivateDemo instance = inv.getTarget();
                instance.setTag("mock");
            }
        };
        String tag = PrivateDemo.instance().getTag();
        Assert.assertEquals(tag, "mock");
    }
}