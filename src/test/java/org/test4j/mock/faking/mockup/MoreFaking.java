package org.test4j.mock.faking.mockup;

public class MoreFaking {
    public MoreFaking(String test) {
    }

    public static class Collaborator {
        static Object xyz;
        protected int value;

        public Collaborator() {
        }

        Collaborator(int value) {
            this.value = value;
        }

        protected static String doInternal() {
            return "123";
        }

        public void provideSomeService() {
            throw new RuntimeException("Real provideSomeService() called");
        }

        public int getValue() {
            return value;
        }

        void setValue(int value) {
            this.value = value;
        }

        protected long getThreadSpecificValue(int i) {
            return Thread.currentThread().getId() + i;
        }
    }

    public static class SubCollaborator extends Collaborator {
        public SubCollaborator(int i) {
            throw new RuntimeException(String.valueOf(i));
        }

        @Override
        public void provideSomeService() {
            value = 123;
        }
    }

    public static final class ClassWithNested {
        public static void doSomething() {
        }

        public static final class Nested {
        }
    }

    static class GenericClass<T> {
        protected T doSomething() {
            return null;
        }
    }
}