package org.test4j.mock.faking.mockup;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import javax.accessibility.AccessibleState;

import static org.junit.Assert.*;
import static org.test4j.mock.faking.mockup.MoreFaking.*;

public final class MoreFakingTest {
    final CodeUnderTest codeUnderTest = new CodeUnderTest();
    boolean fakeExecuted;

    static class CodeUnderTest {
        private final Collaborator dependency = new Collaborator();

        void doSomething() {
            dependency.provideSomeService();
        }

        long doSomethingElse(int i) {
            return dependency.getThreadSpecificValue(i);
        }
    }


    static class FakeCollaborator1 extends MockUp<Collaborator> {
        @Mock
        void provideSomeService() {
        }
    }

    class FakeCollaborator4 extends MockUp<Collaborator> {
        @Mock
        void $init() {
            fakeExecuted = true;
        }

        @Mock
        void provideSomeService() {
        }
    }

    static class FakeCollaboratorWithReentrantFakeMethod extends MockUp<Collaborator> {
        @Mock
        int getValue() {
            return 123;
        }

        @Mock
        void provideSomeService(Invocation inv) {
            inv.proceed();
        }
    }

    static class FakeCollaboratorWithConstructorFake extends MockUp<MoreFaking> {
        @Mock
        void $init(String value) {
            assertEquals("test", value);
        }
    }

    public static class FakeThread extends MockUp<Thread> {
        boolean interrupted;

        @Mock
        public void interrupt() {
            interrupted = true;
        }
    }

    static class FakeSubCollaborator extends MockUp<SubCollaborator> {
        @Mock
        void $init(int i) {
        }

        @Mock
        String doInternal() {
            return "faked";
        }

        @Mock
        int getValue() {
            return 123;
        }
    }

    @Test
    public void fakeDoingNothing() {
        new FakeCollaborator1();

        codeUnderTest.doSomething();
    }

    @Test
    public void applyFakesFromInnerFakeClassWithFakeConstructor() {
        new FakeCollaborator4();
        assertFalse(fakeExecuted);

        new CodeUnderTest().doSomething();

        assertTrue(fakeExecuted);
    }

    @Test
    public void applyReentrantFake() {
        new FakeCollaboratorWithReentrantFakeMethod();
        Assertions.assertThrows(RuntimeException.class, () ->
            codeUnderTest.doSomething());
    }

    @Test
    public void applyFakeForConstructor() {
        new FakeCollaboratorWithConstructorFake();

        new MoreFaking("test");
    }

    @Test
    public void applyFakeForClassHierarchy() {
        new MockUp<SubCollaborator>() {
            @Mock
            void $init(Invocation inv, int i) {
                assertNotNull(inv.getTarget());
                assertTrue(i > 0);
            }

            @Mock
            void provideSomeService(Invocation inv) {
                SubCollaborator it = inv.getTarget();
                it.value = 45;
            }

            @Mock
            int getValue(Invocation inv) {
                SubCollaborator it = inv.getTarget();
                assertNotNull(it);
                return 123;
            }
        };

        SubCollaborator collaborator = new SubCollaborator(123);
        collaborator.provideSomeService();
        assertEquals(45, collaborator.value);
        assertEquals(123, collaborator.getValue());
    }

    @DisplayName("对java.lang下类mock无效")
    @Test
    public void applyFakeForJREClass() {
        FakeThread fakeThread = new FakeThread();
        Thread.currentThread().interrupt();
        Assertions.assertThrows(AssertionError.class, () ->
            assertTrue(fakeThread.interrupted));
    }

    @Test
    public void fakeStaticInitializer() {
        new MockUp<AccessibleState>() {
            @Mock
            void $clinit() {
            }
        };
        assertNull(AccessibleState.ACTIVE);
    }

    @Test
    public void fakeGenericClassWithFakeHavingInvocationParameter() {
        new MockUp<GenericClass<String>>() {
            @Mock
            String doSomething(Invocation inv) {
                return "faked";
            }
        };

        GenericClass<String> faked = new GenericClass<>();
        assertEquals("faked", faked.doSomething());
    }

    @Test
    public void concurrentFake() throws Exception {
        new MockUp<Collaborator>() {
            @Mock
            long getThreadSpecificValue(int i) {
                return Thread.currentThread().getId() + 123;
            }
        };

        Thread[] threads = new Thread[5];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread() {
                @Override
                public void run() {
                    long threadSpecificValue = Thread.currentThread().getId() + 123;
                    long actualValue = new CodeUnderTest().doSomethingElse(0);
                    assertEquals(threadSpecificValue, actualValue);
                }
            };
        }

        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
    }

    @Test
    public void fakeAffectsInstancesOfSpecifiedSubclassAndNotOfBaseClass() {
        new FakeSubCollaborator();

        assertEquals("faked", SubCollaborator.doInternal());
        assertEquals(123, new SubCollaborator(5).getValue());

        // 在子类上声明的mock对基类无效, 比较mock的声明类和实际类
        assertEquals("faked", Collaborator.doInternal());
        assertEquals(62, new Collaborator(62).getValue());
    }

    @Test
    public void fakeAClassHavingANestedClass() {
        new MockUp<ClassWithNested>() {
            @Mock
            void doSomething() {
            }
        };
        Class<?> outerClass = ClassWithNested.Nested.class.getDeclaringClass();
        assertSame(ClassWithNested.class, outerClass);
    }
}