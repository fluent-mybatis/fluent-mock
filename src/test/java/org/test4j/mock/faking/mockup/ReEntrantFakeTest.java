package org.test4j.mock.faking.mockup;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public final class ReEntrantFakeTest {
    public static class RealClass {
        public String foo() {
            return "real value";
        }

        protected static int staticRecursiveMethod(int i) {
            return i <= 0 ? 0 : 2 + staticRecursiveMethod(i - 1);
        }

        public int recursiveMethod(int i) {
            return i <= 0 ? 0 : 2 + recursiveMethod(i - 1);
        }

        protected static int nonRecursiveStaticMethod(int i) {
            return -i;
        }

        public int nonRecursiveMethod(int i) {
            return -i;
        }
    }

    public static class AnnotatedFakeClass extends MockUp<RealClass> {
        private static Boolean fakeIt;

        @Mock
        public String foo(Invocation inv) {
            if (fakeIt == null) {
                throw new IllegalStateException("null fakeIt");
            } else if (fakeIt) {
                return "fake value";
            } else {
                return inv.proceed();
            }
        }
    }

    @Test
    public void callFakeMethod() {
        new AnnotatedFakeClass();
        AnnotatedFakeClass.fakeIt = true;
        String foo = new RealClass().foo();
        assertEquals("fake value", foo);
    }

    @Test
    public void callOriginalMethod() {
        new AnnotatedFakeClass();
        AnnotatedFakeClass.fakeIt = false;

        String foo = new RealClass().foo();
        assertEquals("real value", foo);
    }

    @Test
    public void calledFakeThrowsException() {
        new AnnotatedFakeClass();
        AnnotatedFakeClass.fakeIt = null;
        Assertions.assertThrows(IllegalStateException.class, () ->
            new RealClass().foo());
    }


    static class MultiThreadedFake extends MockUp<RealClass> {
        private static boolean nobodyEntered = true;

        @Mock
        public String foo(Invocation inv) throws InterruptedException {
            String value = inv.proceed();

            synchronized (MultiThreadedFake.class) {
                if (nobodyEntered) {
                    System.out.println("---true--");
                    nobodyEntered = false;
                    MultiThreadedFake.class.wait(100);
                } else {
                    System.out.println("---false--");
                    MultiThreadedFake.class.notifyAll();
                }
            }
            return value.replace("real", "fake");
        }
    }

    @Test
    @Timeout(1000)
    public void twoConcurrentThreadsCallingTheSameReentrantFake() throws Exception {
        new MultiThreadedFake();

        final StringBuilder first = new StringBuilder();
        final StringBuilder second = new StringBuilder();

        Thread thread1 = new Thread(() -> first.append(new RealClass().foo()));
        thread1.start();

        Thread thread2 = new Thread(() -> second.append(new RealClass().foo()));
        thread2.start();

        thread1.join();
        thread2.join();

        assertEquals("fake value", first.toString());
        assertEquals("fake value", second.toString());
    }

    public static final class RealClass2 {
        public int firstMethod() {
            return 1;
        }

        public int secondMethod() {
            return 2;
        }
    }

    @Test
    public void reentrantFakeForNonJREClassWhichCallsAnotherFromADifferentThread() {
        new MockUp<RealClass2>() {
            int value;

            @Mock
            int firstMethod(Invocation inv) {
                return inv.proceed();
            }

            @Mock
            int secondMethod(Invocation inv) throws InterruptedException {
                final RealClass2 it = inv.getTarget();

                Thread t = new Thread() {
                    @Override
                    public void run() {
                        value = it.firstMethod();
                    }
                };
                t.start();
                t.join();
                return value;
            }
        };

        RealClass2 r = new RealClass2();
        assertEquals(1, r.firstMethod());
        assertEquals(1, r.secondMethod());
    }

    @Test
    public void reentrantFakeForJREClassWhichCallsAnotherFromADifferentThread() {
        System.setProperty("a", "1");
        System.setProperty("b", "2");

        new MockUp<System>() {
            String property;

            @Mock
            String getProperty(Invocation inv, String key) {
                return inv.proceed();
            }

            @Mock
            String clearProperty(final String key) throws InterruptedException {
                Thread t = new Thread() {
                    @Override
                    public void run() {
                        property = System.getProperty(key);
                    }
                };
                t.start();
                t.join();
                return property;
            }
        };

        assertEquals("1", System.getProperty("a"));
        assertEquals("2", System.clearProperty("b"));
    }

    public static final class RealClass3 {
        public RealClass3 newInstance() {
            return new RealClass3();
        }
    }

    @Test
    public void reentrantFakeForMethodWhichInstantiatesAndReturnsNewInstanceOfTheFakedClass() {
        new MockUp<RealClass3>() {
            @Mock
            RealClass3 newInstance(Invocation inv) {
                return null;
            }
        };

        assertNull(new RealClass3().newInstance());
    }

    public static final class FakeClassWithReentrantFakeForRecursiveMethod extends MockUp<RealClass> {
        @Mock
        int recursiveMethod(Invocation inv, int i) {
            int j = inv.proceed();
            return 1 + j;
        }

        @Mock
        static int staticRecursiveMethod(Invocation inv, int i) {
            int j = inv.proceed();
            return 1 + j;
        }
    }

    @Test
    public void reentrantFakeMethodForRecursiveMethods() {
        assertEquals(0, RealClass.staticRecursiveMethod(0));
        assertEquals(2, RealClass.staticRecursiveMethod(1));

        RealClass r = new RealClass();
        assertEquals(0, r.recursiveMethod(0));
        assertEquals(2, r.recursiveMethod(1));

        new FakeClassWithReentrantFakeForRecursiveMethod();

        assertEquals(1, RealClass.staticRecursiveMethod(0));
        assertEquals(1 + 2 + 1, RealClass.staticRecursiveMethod(1));
        assertEquals(1, r.recursiveMethod(0));
        assertEquals(4, r.recursiveMethod(1));
    }

    @Test
    public void fakeThatProceedsIntoRecursiveMethod() {
        RealClass r = new RealClass();
        assertEquals(0, r.recursiveMethod(0));
        assertEquals(2, r.recursiveMethod(1));

        new MockUp<RealClass>() {
            @Mock
            int recursiveMethod(Invocation inv, int i) {
                int ret = inv.proceed();
                return 1 + ret;
            }
        };

        assertEquals(1, r.recursiveMethod(0));
        assertEquals(4, r.recursiveMethod(1));
    }

    @Test
    public void recursiveFakeMethodWithoutInvocationParameter() {
        new MockUp<RealClass>() {
            @Mock
            int nonRecursiveStaticMethod(int i) {
                if (i > 1) return i;
                return RealClass.nonRecursiveStaticMethod(i + 1);
            }
        };

        int result = RealClass.nonRecursiveStaticMethod(1);
        assertEquals(2, result);
    }

    @Test
    public void recursiveFakeMethodWithInvocationParameterNotUsedForProceeding() {
        new MockUp<RealClass>() {
            @Mock
            int nonRecursiveMethod(Invocation inv, int i) {
                if (i > 1) return i;
                RealClass it = inv.getTarget();
                return it.nonRecursiveMethod(i + 1);
            }
        };

        int result = new RealClass().nonRecursiveMethod(1);
        assertEquals(2, result);
    }

    @Test
    public void nonRecursiveFakeMethodWithInvocationParameterUsedForProceeding() {
        new MockUp<RealClass>() {
            @Mock
            int nonRecursiveMethod(Invocation inv, int i) {
                if (i > 1) return i;
                return inv.proceed(i + 1);
            }
        };

        int result = new RealClass().nonRecursiveMethod(1);
        assertEquals(-2, result);
    }
}