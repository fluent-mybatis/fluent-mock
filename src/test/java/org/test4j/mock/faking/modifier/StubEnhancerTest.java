package org.test4j.mock.faking.modifier;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;
import org.test4j.mock.Stubs;
import org.test4j.mock.faking.mockup.FakingBaseTypesTest.IAction;

class StubEnhancerTest {
    int[] count = {0};

    @Test
    void stub() {
        new MockUp<IAction>() {
            @Mock
            public boolean notToBeFaked(Invocation inv) {
                Object target = inv.getTarget();
                Assertions.assertEquals(true, target instanceof IAction);
                count[0] = inv.getInvokedTimes();
                return true;
            }
        };
        IAction action = Stubs.fake(IAction.class);
        int i = action.perform(1);
        Assertions.assertEquals(100, i);

        boolean r = action.notToBeFaked();
        Assertions.assertEquals(true, r);
        action.notToBeFaked();

        Assertions.assertEquals(2, count[0]);
    }
}