package org.test4j.mock.faking.util;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TypeUtilityTest {
    List<String> list = new ArrayList<>();

    @Test
    public void getGenericClassSignature() throws Exception {
        String signature = TypeUtility.getTypeSignature(Object.class);
        Assert.assertTrue(signature.equals("Ljava/lang/Object;"));

        Field field = TypeUtilityTest.class.getDeclaredField("list");
        signature = TypeUtility.getTypeSignature(field.getGenericType());
        Assert.assertTrue(signature.equals("Ljava/util/List<Ljava/lang/String;>;"));
    }

    @Test
    void getShortClassName() {
        String name = TypeUtility.getShortClassName(int.class.getName());
        Assert.assertEquals("Integer", name);

        name = TypeUtility.getShortClassName(new int[0].getClass().getName());
        Assert.assertEquals("int[]", name);
    }

    @Test
    void testGetShortClassName() {
        String name = TypeUtility.getShortClassName(Map.Entry.class.getName());
        Assert.assertEquals("Map$Entry", name);
    }
}