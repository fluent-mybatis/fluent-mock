package org.test4j.mock.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;

@Mocks(MyServiceImpl.class)
public class AssertAroundTest {
    AssertAroundTestMocks mocks = new AssertAroundTestMocks();

    @Test
    void test_assertBefore() {
        int[] expected = {0};
        mocks.MyServiceImpl().sayHello
            .assertParameters(invocation -> {
                String name = invocation.arg(0);
                Assertions.assertEquals("are u ok!", name);

                expected[0] = 1;
            });
        new MyServiceImpl<>().sayHello("are u ok!");
        Assertions.assertEquals(1, expected[0]);
    }

    @Test
    void test_assertBefore_fail() {
        mocks.MyServiceImpl().sayHello
            .assertParameters(invocation -> {
                String name = invocation.arg(0);
                Assertions.assertEquals("", name);
            });
        Assertions.assertThrows(AssertionError.class, () ->
            new MyServiceImpl<>().sayHello("are u ok!"));
    }

    @Test
    void test_assertAfter() {
        int[] expected = {0};
        mocks.MyServiceImpl().sayHello
            .assertResult((inv, result) -> {
                Assertions.assertEquals("hello:are u ok!", result);
                expected[0] = 1;
            });
        new MyServiceImpl<>().sayHello("are u ok!");
        Assertions.assertEquals(1, expected[0]);
    }

    @Test
    void test_assertAfter_fail() {
        mocks.MyServiceImpl().sayHello
            .assertResult((inv, result) -> {
                Assertions.assertEquals("hello:", result);
            });
        Assertions.assertThrows(AssertionError.class, () ->
            new MyServiceImpl<>().sayHello("are u ok!"));
    }
}