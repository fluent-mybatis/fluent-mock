package org.test4j.mock.fluent;

import java.util.Observable;
import java.util.Observer;

public class BaseService implements Observer {
    public String baseMethod() {
        return "real";
    }

    @Override
    public void update(Observable o, Object arg) {
    }
}