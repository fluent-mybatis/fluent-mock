package org.test4j.mock.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.mock.faking.fluent.FluentMockUp;
import org.test4j.mock.faking.util.TypeUtility;

public class DuplicateMockTest {

    String name1 = "sayHello";
    String name2 = "sayHello2";
    String desc = "(Ljava/lang/String;)Ljava/lang/String;";

    @DisplayName("对一个类重复mock")
    @Test
    void test1() {
        String realClass = TypeUtility.classPath(MyServiceImpl.class);
        FluentMockUp mockUp1 = new FluentMockUp(MyServiceImpl.class);
        mockUp1.mockReturn(realClass, name1, desc, 0, "my mock1");
        mockUp1.mockReturn(realClass, name2, desc, 0, "my mock12");
        mockUp1.apply();

        String result = new MyServiceImpl().sayHello2("");
        Assertions.assertEquals("my mock12", result);

        FluentMockUp mockUp2 = new FluentMockUp(MyServiceImpl.class);
        mockUp2.mockReturn(realClass, name2, desc, 0, "my mock22");
        mockUp2.apply();

        result = new MyServiceImpl().sayHello("");
        Assertions.assertEquals("my mock1", result);
        result = new MyServiceImpl().sayHello2("");
        Assertions.assertEquals("my mock22", result);
    }
}