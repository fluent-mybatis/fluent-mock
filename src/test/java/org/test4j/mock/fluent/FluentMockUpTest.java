package org.test4j.mock.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.mock.faking.fluent.FluentMockUp;
import org.test4j.mock.faking.util.TypeUtility;

@SuppressWarnings({"rawtypes"})
public class FluentMockUpTest {
    String name = "sayHello";
    String desc = "(Ljava/lang/String;)Ljava/lang/String;";

    @Test
    void test1() {
        String realClass = TypeUtility.classPath(MyServiceImpl.class);
        new FluentMockUp<MyServiceImpl>() {
            @Override
            protected void init() {
                this.mockMethod(realClass, name, desc, 1, inv -> "mockUp2 test");
                this.mockReturn(realClass, name, desc, 2, "my result");
                this.mockThrows(realClass, name, desc, 3, new RuntimeException("my exception"));
            }
        }.apply();
        String result = new MyServiceImpl().sayHello("test");
        Assertions.assertEquals("mockUp2 test", result);

        result = new MyServiceImpl().sayHello("test");
        Assertions.assertEquals("my result", result);

        Assertions.assertThrows(RuntimeException.class, () -> new MyServiceImpl().sayHello("test"), "my exception");

        result = new MyServiceImpl().sayHello("test");
        Assertions.assertEquals("hello:test", result);
    }

    @DisplayName("无返回值方法mock验证")
    @Test
    public void testNoReturn() {
        int[] verifies = {0};
        String realClass = TypeUtility.classPath(MyServiceImpl.class);
        new FluentMockUp<MyServiceImpl>() {
            @Override
            protected void init() {
                this.mockMethod(realClass, "noReturn", "([[Ljava/lang/String;)V", 1, inv -> {
                    System.out.println(inv.arg(0) + " with test");
                    verifies[0] = 1;
                    return null;
                });
            }
        }.apply();
        new MyServiceImpl().noReturn(null);
        Assertions.assertEquals(1, verifies[0]);
    }
}