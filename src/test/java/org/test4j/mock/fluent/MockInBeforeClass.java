package org.test4j.mock.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;

@Mocks({MyServiceImpl.class})
public class MockInBeforeClass {
    static {
        doItInStaticMethod();
    }

    @BeforeAll
    static void setup() {
        doItInStaticMethod();
    }

    /**
     * 在静态方法块中使用fluent mock方式需要报错
     */
    private static void doItInStaticMethod() {
        try {
            MockInBeforeClassMocks.mocks.MyServiceImpl.sayHello2.thenReturn(null, null);
            Assertions.fail();
        } catch (IllegalStateException e) {
            Assertions.assertTrue(e.getMessage()
                .contains("Behaviors of FluentMockUp can only be applied in test methods."));
        }
    }

    @Test
    void test2() {
    }
}