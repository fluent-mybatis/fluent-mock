package org.test4j.mock.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;
import org.test4j.mock.MockUp;

@Mocks({MyServiceImpl.class})
public class MockInGlobal {

    @SuppressWarnings("all")
    @DisplayName("在global mock块中使用fluent mock方式需要报错")
    @Test
    void test2() {
        MockUp.global(() -> {
            try {
                MockInBeforeClassMocks.mocks.MyServiceImpl.sayHello2.thenReturn(null, null);
                Assertions.fail();
            } catch (IllegalStateException e) {
                Assertions.assertTrue(e.getMessage()
                    .contains("Behaviors of FluentMockUp can't be applied in global mock, please use new MockUp() style."));
            }
        });
    }
}