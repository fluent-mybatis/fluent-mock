package org.test4j.mock.fluent;

import java.io.Serializable;
import java.util.List;

public class MyServiceImpl<A, L extends Serializable & List> extends BaseService {
    public String sayHello(String name) {
        return "hello:" + name;
    }

    public String sayHello2(String name) {
        return "hello:" + name;
    }

    public void noReturn(String[][] arrays) {
        System.out.println("====");
    }

    public <A> void noReturn(List<A> list, String[][] arrays) {
        System.out.println("====");
    }

    public <B, C extends List<B>> A allGenericType(L list, B b, C c) {
        return null;
    }

    int[] array(short[] arg1) {
        return null;
    }

    int[][] array2(short[] arg1) {
        return null;
    }
}