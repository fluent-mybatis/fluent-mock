package org.test4j.mock.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;

/**
 * 对MyServiceImpl这个类进行mock
 */
@Mocks({MyServiceImpl.class})
public class MyServiceImplTest {
    private MyServiceImplTestMocks mocks = new MyServiceImplTestMocks();

    @Test
    public void test1() {
        mocks.MyServiceImpl(m -> {
            m.allGenericType.thenReturn("abc");
        });
        String result = (String) new MyServiceImpl().allGenericType(null, null, null);
        Assertions.assertEquals("abc", result);
    }

    @DisplayName("子类声明的mock只对当前子类有效，对其它子类无效")
    @Test
    public void test2() {
        mocks.MyServiceImpl().super$.baseMethod.thenReturn("faked");

        String result2 = new BaseService().baseMethod();
        Assertions.assertEquals("real", result2);
        String result1 = new MyServiceImpl().baseMethod();
        Assertions.assertEquals("faked", result1);
    }

    @DisplayName("验证fluent mock对指定实例进行mock")
    @Test
    public void test3() {
        /** 无差别创建3个实例**/
        MyServiceImpl myService1 = new MyServiceImpl();
        MyServiceImpl myService2 = new MyServiceImpl();
        MyServiceImpl myService3 = new MyServiceImpl();
        /** 只对实例1和3进行行为指定, 放过实例2 **/
        mocks.MyServiceImpl(myService1).sayHello2.thenReturn("1");
        mocks.MyServiceImpl(myService3).sayHello2.thenReturn("3");

        String result1 = myService1.sayHello2("m");
        String result2 = myService2.sayHello2("m");
        String result3 = myService3.sayHello2("m");
        /** 验证接口行为 **/
        Assertions.assertEquals("1", result1);
        Assertions.assertEquals("hello:m", result2);
        Assertions.assertEquals("3", result3);
    }

    @Test
    public void intArr() {
        mocks.MyServiceImpl().array.restReturn(new int[]{1, 2, 3});
        int[] result = new MyServiceImpl<>().array(null);
        Assertions.assertEquals(1, result[0]);

        mocks.MyServiceImpl().array2.restReturn(new int[][]{{1}, {2}});
        int[][] result2 = new MyServiceImpl().array2(null);
        Assertions.assertEquals(2, result2[1][0]);
    }
}