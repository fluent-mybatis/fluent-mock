package org.test4j.mock.fluent;

import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;

import java.util.Objects;

@SuppressWarnings("rawtypes")
@Mocks({MyServiceImpl.class})
public class MyServiceTest_Independent {
    static final MyServiceTest_IndependentMocks mocks = MyServiceTest_IndependentMocks.mocks;

    @Test
    void test1() {
        mocks.MyServiceImpl.sayHello.thenReturn("mock value");
        String value1 = new MyServiceImpl().sayHello("name");
        assert Objects.equals(value1, "mock value");
    }

    @Test
    void test2() {
        mocks.MyServiceImpl.sayHello2.thenReturn("xx");
        String value1 = new MyServiceImpl().sayHello("name");
        assert Objects.equals(value1, "hello:name");

        String value2 = new MyServiceImpl().sayHello2("test");
        assert Objects.equals(value2, "xx") : "not xx, but: " + value2;
    }
}