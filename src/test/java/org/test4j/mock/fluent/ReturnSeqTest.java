package org.test4j.mock.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;

@Mocks(MyServiceImpl.class)
public class ReturnSeqTest {
    ReturnSeqTestMocks mocks = new ReturnSeqTestMocks();

    @Test
    void test_seqNo() {
        mocks.MyServiceImpl().sayHello
            .thenAnswer(new int[]{4}, inv -> "test")
            .thenThrows(new int[]{2}, new RuntimeException("test"))
            .thenReturn(new int[]{1, 3}, "1", "3")
            .thenReturn("5", "6")
            .restReturn("rest");
        MyServiceImpl service = new MyServiceImpl();
        Assertions.assertEquals("1", service.sayHello(null));
        Assertions.assertThrows(RuntimeException.class, () -> service.sayHello(null));
        Assertions.assertEquals("3", service.sayHello(null));
        Assertions.assertEquals("test", service.sayHello(null));
        Assertions.assertEquals("5", service.sayHello(null));
        Assertions.assertEquals("6", service.sayHello(null));
        for (int i = 0; i < 10; i++) {
            Assertions.assertEquals("rest", service.sayHello(null));
        }
    }

    @Test
    void test_answer() {
        mocks.MyServiceImpl().sayHello
            .thenAnswer(1, inv -> "1")
            .thenAnswer(2, inv -> {
                throw new RuntimeException("test");
            })
            .restAnswer(inv -> "3");
        Assertions.assertEquals("1", new MyServiceImpl<>().sayHello("1"));
        try {
            new MyServiceImpl<>().sayHello("1");
            new AssertionError("fail");
        } catch (RuntimeException e) {
            Assertions.assertEquals("test", e.getMessage());
        }
        Assertions.assertEquals("3", new MyServiceImpl<>().sayHello("1"));
    }

    @Test
    void test_throws() {
        mocks.MyServiceImpl().sayHello
            .thenThrows(new RuntimeException("test1"), new RuntimeException("test2"))
            .restThrows(new RuntimeException("test3"));
        try {
            new MyServiceImpl<>().sayHello("1");
            new AssertionError("fail");
        } catch (RuntimeException e) {
            Assertions.assertEquals("test1", e.getMessage());
        }
        try {
            new MyServiceImpl<>().sayHello("1");
            new AssertionError("fail");
        } catch (RuntimeException e) {
            Assertions.assertEquals("test2", e.getMessage());
        }
        try {
            new MyServiceImpl<>().sayHello("1");
            new AssertionError("fail");
        } catch (RuntimeException e) {
            Assertions.assertEquals("test3", e.getMessage());
        }
    }
}