package org.test4j.mock.nested;

public class NestedChild extends NestedParent {
    public NestedChild() {
    }

    public NestedChild(String name) {
        super(name, 20);
    }

    @Override
    public String method1() {
        return "child:method1->" + super.method1();
    }

    @Override
    public String method2(String name) {
        return "child:method2->" + this.method1();
    }
}