package org.test4j.mock.nested;

public class NestedParent {
    public NestedParent() {

    }

    public NestedParent(String name) {

    }

    public NestedParent(String name, int number) {

    }

    public String method1() {
        return "parent:method1";
    }

    public String method2(String name) {
        return "parent:method2->" + this.method1();
    }
}