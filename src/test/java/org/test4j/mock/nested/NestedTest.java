package org.test4j.mock.nested;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

public class NestedTest {
    @Test
    void test_constructor() {
        new MockUp<NestedChild>() {
            @Mock
            void $init(Invocation it, String name, int number) {
                it.proceed();
            }
        };
        new NestedChild("name");
    }

    @Test
    void test_method() {
        new MockUp<NestedChild>() {
            @Mock
            String method1(Invocation invocation) {
                return "mock method1->" + invocation.proceed();
            }

            @Mock
            String method2(Invocation invocation, String name) {
                return "mock method2->" + invocation.proceed();
            }
        };
        String result = new NestedChild().method2("test");
        Assertions.assertEquals("mock method2->child:method2->mock method1->child:method1->parent:method1", result);
    }
}