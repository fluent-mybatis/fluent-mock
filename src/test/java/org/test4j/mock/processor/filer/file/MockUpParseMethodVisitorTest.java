package org.test4j.mock.processor.filer.file;

import g_asm.org.objectweb.asm.Type;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.mock.MockListener;
import org.test4j.mock.faking.meta.MethodId;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class MockUpParseMethodVisitorTest {
    @Test
    void test_ParaNameType() {
        MockClassFiler filer = new MockClassFiler(MockListener.class);

        List<MethodId> list = filer.declaredMethodIds.stream().filter(m -> m.name.equals("afterExecute")).collect(Collectors.toList());
        Map<Integer, MethodId.ParaNameType> types = list.get(0).getParameters();
        Assertions.assertEquals(3, types.size());
    }

    @DisplayName("java内置类,没有visitLocalVariable,参数名称直接构造")
    @Test
    void test_ParaNameType2() {
        MockClassFiler filer = new MockClassFiler(HashMap.class);

        List<MethodId> list = filer.declaredMethodIds.stream().filter(m -> m.name.equals("getOrDefault")).collect(Collectors.toList());
        Map<Integer, MethodId.ParaNameType> types = list.get(0).getParameters();
        Assertions.assertEquals(2, types.size());
        MethodId.ParaNameType nameType =  types.get(0);
        Type pType = Type.getType(nameType.type);
        String name =pType.getClassName();
    }

}