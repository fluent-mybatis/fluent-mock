package org.test4j.mock.stubs;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.test4j.annotations.Mocks;
import org.test4j.mock.Stubs;
import org.test4j.mock.faking.util.TypeUtility;
import org.test4j.mock.stubs.StubTest.IService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observer;

@Mocks(FluentStubsTest.MyNestedService.class)
public class FluentStubsTest {
    FluentStubsTestMocks mocks = new FluentStubsTestMocks();

    @DisplayName("验证不可见(package)接口类的mock")
    @Test
    void testAbstract() {
        mocks.MyNestedService(mock -> {
            mock.method1$.restAnswer(inv -> "I am an abstract method.");
        });
        MyNestedService stub = Stubs.fake(MyNestedService.class, 1, new Integer[]{});
        stub.method2();
        String result = stub.method1();
        Assert.assertEquals("I am an abstract method.", result);
    }

    public static abstract class MyNestedService implements IService, Observer {
        public MyNestedService(int i, Object[] a) {
        }
    }

    @ParameterizedTest
    @MethodSource("data_isAssignable")
    void test_isAssignable(Class pType, Class aType, boolean expected) {
        boolean actual = TypeUtility.isAssignable(pType, aType);
        Assertions.assertEquals(expected, actual);
    }

    static Iterator<Object[]> data_isAssignable() {
        return new ArrayList<Object[]>() {
            {
                this.add(new Object[]{Object.class, int.class, true});
                this.add(new Object[]{Integer.class, int.class, true});
                this.add(new Object[]{int.class, Integer.class, true});
                this.add(new Object[]{Object.class, int[].class, true});
                this.add(new Object[]{Object.class, Integer[].class, true});
                this.add(new Object[]{Object[].class, Integer[].class, true});
                this.add(new Object[]{Object[].class, int[].class, false});
            }
        }.iterator();
    }
}