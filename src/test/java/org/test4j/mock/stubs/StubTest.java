package org.test4j.mock.stubs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;
import org.test4j.mock.Stubs;
import org.testng.annotations.Test;

public class StubTest {
    @DisplayName("验证Stub功能")
    @Test
    void testEnhancer() {
        new MockUp<IService>() {
            @Mock
            String method1() {
                return "test1";
            }

            @Mock
            String method2() {
                return "test2";
            }
        };
        IService service = Stubs.fake(IService.class);
        Assertions.assertEquals("test1", service.method1());
        Assertions.assertEquals("test2", service.method2());
    }

    @DisplayName("抽象方法mock不改变字节码, 只对stub有效")
    @Test
    void testEnhancer2() {
        new MockUp<IService>() {
            @Mock
            String method1() {
                return "test1";
            }

            @Mock
            String method2() {
                return "test2";
            }
        };
        IService service1 = Stubs.fake(IService.class);
        Assertions.assertEquals("test1", service1.method1());
        Assertions.assertEquals("test2", service1.method2());

        IService service2 = () -> "inner";
        Assertions.assertEquals("inner", service2.method1());
        Assertions.assertEquals("test2", service2.method2());
    }

    public interface IService {
        String method1();

        default String method2() {
            return "default";
        }
    }
}