package org.test4j.mock.stubs;

import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.List;

import static org.test4j.mock.faking.util.TypeUtility.findInterfaceMethod;

public class TypeUtilityTest {
    @DisplayName("验证findInterfaceMethod只会返回未实现的抽象方法列表")
    @Test
    void test_findInterfaceMethod() {
        List<Method> methods = findInterfaceMethod(ServiceImpl.class);
        Assert.assertEquals(1, methods.size());
        Assert.assertEquals("method1", methods.get(0).getName());
    }

    public static abstract class ServiceImpl extends BaseService implements IService {
        @Override
        public void method3() {
        }
    }

    public static abstract class BaseService {
        protected abstract void method4();

        public abstract void method5();

        public void method6() {
        }
    }

    interface IService {
        void method1();

        default void method2() {
        }

        void method3();

        void method6();
    }
}