package org.test4j.moduel.inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.fortest.inject.User;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;

public class FakeTargetTest {
    @Test
    public void test() {
        User user1 = new User();
        new MockUp(User.class, user1) {
            @Mock
            public String getFirst() {
                return "test";
            }
        };
        String result = new User().setFirst("wu").getFirst();
        Assertions.assertEquals("wu", result);
        result = user1.setFirst("wu").getFirst();
        Assertions.assertEquals("test", result);
    }
}