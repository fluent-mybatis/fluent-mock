package org.test4j.moduel.inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.fortest.inject.User;
import org.test4j.module.inject.Inject;
import org.test4j.module.inject.Injected;

/**
 * 类InjectModuleTest.java的实现描述
 *
 * @author darui.wudr 2013-1-9 下午2:50:26
 */
public class InjectModuleTest {
    @Injected
    User user = new User();

    @Inject
    User assistor = new User("he", "a111");

    @Inject
    String first = "wu";

    @Test
    public void testFindTestedObjectTargets() throws Exception {
        Assertions.assertEquals("wu", user.getFirst());
        Assertions.assertEquals("he", user.getAssistor().getFirst());
        Assertions.assertEquals("a111", user.getAssistor().getLast());
    }
}