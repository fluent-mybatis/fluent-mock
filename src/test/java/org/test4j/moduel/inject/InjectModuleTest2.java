package org.test4j.moduel.inject;

import org.junit.jupiter.api.Test;
import org.test4j.module.inject.Inject;


public class InjectModuleTest2 {

    OuterClaz outer = new OuterClaz();

    @Inject(targets = "outer")
    InnerClaz inner;

    @Test
    public void testTest4JInject() {
        Object o1 = outer.inner;
        try {
            o1.toString();
            throw new AssertionError("fail");
        } catch (NullPointerException e) {
        }

        this.inner = new InnerClaz();
        o1.toString();
    }

    private static class OuterClaz {
        InnerClaz inner;
    }

    private static class InnerClaz {

    }
}