package org.test4j.moduel.inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.fortest.inject.User;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;
import org.test4j.module.inject.Inject;
import org.test4j.module.inject.Injected;

/**
 * 类InjectModuleTest.java的实现描述
 *
 * @author darui.wudr 2013-1-9 下午2:50:26
 */
public class InjectModuleTest3 {
    @Injected
    User user = new User().setFirst("wu");

    @Inject(stub = true)
    User assistor;

    @Test
    public void testFindTestedObjectTargets() throws Exception {
        new MockUp(User.class, assistor) {
            @Mock
            public String getFirst() {
                return "he";
            }

            @Mock
            public String getLast() {
                return "a111";
            }
        };
        new MockUp<User>() {
            @Mock
            public String getFirst() {
                return "test";
            }
        };
        Assertions.assertEquals("test", user.getFirst());
        Assertions.assertEquals("he", user.getAssistor().getFirst());
        Assertions.assertEquals("a111", user.getAssistor().getLast());
    }
}