package org.test4j.moduel.inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.test4j.fortest.inject.User;
import org.test4j.module.inject.Inject;

public class InjectionModuleHelperTest {
    User user = new User();

    @Inject(targets = "user", properties = "first")
    private final String first = "test user";

    @Test
    public void testInjectInto() throws Exception {
        Assertions.assertEquals("test user", user.getFirst());
    }
}