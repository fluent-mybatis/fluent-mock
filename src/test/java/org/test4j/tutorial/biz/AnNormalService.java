package org.test4j.tutorial.biz;

import lombok.Getter;
import lombok.Setter;

/**
 * 一个普通的业务类, 有各式限定方法
 */
@Getter
@Setter
public class AnNormalService {
    /**
     * 静态变量
     */
    private static int staticInt;
    /**
     * 成员变量
     */
    private int anInt;

    /**
     * 静态代码块
     */
    static {
        setStaticInt(200);
    }

    public AnNormalService() {
    }

    /**
     * 构造函数
     *
     * @param anInt
     */
    public AnNormalService(int anInt) {
        this.anInt = anInt;
    }

    /**
     * 静态方法
     *
     * @param num
     */
    public static void setStaticInt(int num) {
        staticInt = num;
    }

    public static int getStaticInt() {
        return staticInt;
    }

    /**
     * 普通方法
     *
     * @return ignore
     */
    public int publicMethod() {
        return this.privateMethod();
    }

    /**
     * private方法
     *
     * @return ignore
     */
    private int privateMethod() {
        return anInt;
    }

    /**
     * final方法
     *
     * @return ignore
     */
    public final int finalMethod() {
        return this.protectedMethod();
    }

    /**
     * protected方法
     *
     * @return ignore
     */
    private int protectedMethod() {
        return anInt;
    }
}
