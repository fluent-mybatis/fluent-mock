package org.test4j.tutorial.biz;

public interface IRemoteService {
    Node getRemoteNode(int num);
}