package org.test4j.tutorial.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 本地负责实现具体业务的业务类
 */
@Component
public class LocalServiceImpl implements ILocalService {
    //外部依赖
    @Autowired
    private IRemoteService remoteService;

    //具体业务处理方法
    @Override
    public Node getRemoteNode(int num) {
        return remoteService.getRemoteNode(num);
    }
}