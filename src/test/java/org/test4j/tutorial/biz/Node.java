package org.test4j.tutorial.biz;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 实体类
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
public class Node {
    private int num;
    private String name;
}