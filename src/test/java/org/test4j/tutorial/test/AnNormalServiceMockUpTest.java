package org.test4j.tutorial.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.mock.Invocation;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;
import org.test4j.tutorial.biz.AnNormalService;

public class AnNormalServiceMockUpTest {

    @BeforeAll
    static void setup() {
        new MockUp<StaticBlock>() {
            /**
             * 对静态代码块的mock
             */
            @Mock
            void $clinit() {
                StaticBlock.setAnInt(300);
            }
        };
    }

    @DisplayName("验证静态代码块mock")
    @Test
    void test1() {
        Assertions.assertEquals(300, StaticBlock.anInt);
    }

    @DisplayName("对构造函数进行mock")
    @Test
    void test2() {
        new MockUp<AnNormalService>() {
            @Mock
            void $init(Invocation inv, int anInt) {
                AnNormalService self = inv.getTarget();
                /**
                 * 成员变量anInt = 输入值 + 200
                 */
                self.setAnInt(anInt + 200);
            }
        };
        int anInt = new AnNormalService(200).publicMethod();
        Assertions.assertEquals(400, anInt);
    }

    @DisplayName("演示对public和final方法mock")
    @Test
    void test3() {
        new MockUp<AnNormalService>() {
            @Mock
            int publicMethod() {
                return 5;
            }

            @Mock
            int finalMethod() {
                return 6;
            }
        };
        AnNormalService service = new AnNormalService(400);
        Assertions.assertEquals(5, service.publicMethod());
        Assertions.assertEquals(6, service.finalMethod());
    }


    @DisplayName("演示对private和protected方法mock")
    @Test
    void test4() {
        new MockUp<AnNormalService>() {
            @Mock
            int privateMethod() {
                return 9;
            }

            @Mock
            int protectedMethod() {
                return 11;
            }
        };
        AnNormalService service = new AnNormalService(400);
        Assertions.assertEquals(9, service.publicMethod());
        Assertions.assertEquals(11, service.finalMethod());
    }

    @DisplayName("演示对静态方法的mock")
    @Test
    void test5() {
        new MockUp<AnNormalService>() {
            @Mock
            int getStaticInt() {
                return 178;
            }
        };
        Assertions.assertEquals(178, AnNormalService.getStaticInt());
    }

    private static class StaticBlock {
        static int anInt = 200;

        static {
            StaticBlock.setAnInt(200);
        }

        public static void setAnInt(int anInt) {
            StaticBlock.anInt = anInt;
        }
    }
}
