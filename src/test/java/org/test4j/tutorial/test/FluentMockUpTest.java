package org.test4j.tutorial.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;

import java.util.Calendar;
import java.util.Locale;

/**
 * 指明在fluent接口中要mock的类, Annotation Processor会根据@Mocks定义
 * 生成Test类+Mocks命名的指导类, 在这里是：FluentMockUpTestMocks
 */
@Mocks({Calendar.class})
public class FluentMockUpTest {
    private FluentMockUpTestMocks mocks = new FluentMockUpTestMocks();

    @DisplayName("使用fluent方式对自带类Calendar的get方法进行定制")
    @Test
    public void testMockUp() {
        mocks.Calendar(faker -> {
            /**
             * 指定要mock get方法, 同时实现get的mock逻辑
             */
            faker.get.restAnswer(inv -> {
                int unit = inv.arg(0);
                switch (unit) {
                    case Calendar.YEAR:
                        return 2017;
                    case Calendar.MONDAY:
                        return 12;
                    case Calendar.DAY_OF_MONTH:
                        return 25;
                    case Calendar.HOUR_OF_DAY:
                        return 7;
                    default:
                        return 0;
                }
            });
        });
        // 从此Calendar的get方法，就沿用你定制过的逻辑，而不是它原先的逻辑。
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        Assertions.assertTrue(cal.get(Calendar.YEAR) == 2017);
        Assertions.assertTrue(cal.get(Calendar.MONDAY) == 12);
        Assertions.assertTrue(cal.get(Calendar.DAY_OF_MONTH) == 25);
        Assertions.assertTrue(cal.get(Calendar.HOUR_OF_DAY) == 7);
        // Calendar的其它方法，不受影响
        Assertions.assertTrue((cal.getFirstDayOfWeek() == Calendar.MONDAY));
    }
}
