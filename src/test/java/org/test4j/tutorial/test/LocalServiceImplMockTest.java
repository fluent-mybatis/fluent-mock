package org.test4j.tutorial.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.test4j.annotations.Mock;
import org.test4j.mock.MockUp;
import org.test4j.mock.Stubs;
import org.test4j.tutorial.biz.ILocalService;
import org.test4j.tutorial.biz.IRemoteService;
import org.test4j.tutorial.biz.Node;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ContextConfiguration(
    classes = {SpringConfig.class}
)
public class LocalServiceImplMockTest {
    @Autowired
    ILocalService localService;

    @Test
    public void testMock() {
        Node target = new Node(1, "target");    //创建一个Node对象作为返回值
        new MockUp<IRemoteService>() {
            /**
             * 当调用IRemoteService#getRemoteNode方法时, 返回target对象
             * @param num
             * @return ignore
             */
            @Mock
            public Node getRemoteNode(int num) {
                return target;
            }
        };
        Node result = localService.getRemoteNode(1);
        /**
         * 可以断言我们得到的返回值其实就是target对象
         */
        assertEquals(target, result);
        assertEquals(1, result.getNum());   //具体属性和我们指定的返回值相同
        assertEquals("target", result.getName());   //具体属性和我们指定的返回值相同
    }
}

@Configuration
@ComponentScan("org.test4j.tutorial.biz")
class SpringConfig {
    @Bean
    public IRemoteService remoteService() {
        return Stubs.fake(IRemoteService.class);
    }
}