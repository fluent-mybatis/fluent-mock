package org.test4j.tutorial.test.fluent;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;
import org.test4j.tutorial.biz.AnNormalService;

@Mocks(AnNormalService.class)
public class AnNormalServiceFluentMockUpTest {
    AnNormalServiceFluentMockUpTestMocks mocks = new AnNormalServiceFluentMockUpTestMocks();

    @DisplayName("对构造函数进行mock")
    @Test
    void test2() {
        mocks.AnNormalService().$init_int.restAnswer(inv -> {
            AnNormalService self = inv.getTarget();
            /**
             * 成员变量anInt = 输入值 + 200
             */
            self.setAnInt((Integer) inv.arg(0) + 200);
            return null;
        });
        int anInt = new AnNormalService(200).publicMethod();
        Assertions.assertEquals(400, anInt);
    }

    @DisplayName("演示对public和final方法mock")
    @Test
    void test3() {
        mocks.AnNormalService().publicMethod.restReturn(5);
        mocks.AnNormalService().finalMethod.restReturn(6);

        AnNormalService service = new AnNormalService(400);
        Assertions.assertEquals(5, service.publicMethod());
        Assertions.assertEquals(6, service.finalMethod());
    }


    @DisplayName("演示对private和protected方法mock")
    @Test
    void test4() {
        mocks.AnNormalService().privateMethod.restReturn(9);
        mocks.AnNormalService().protectedMethod.restReturn(11);

        AnNormalService service = new AnNormalService(400);
        Assertions.assertEquals(9, service.publicMethod());
        Assertions.assertEquals(11, service.finalMethod());
    }

    @DisplayName("演示对静态方法的mock")
    @Test
    void test5() {
        mocks.AnNormalService().getStaticInt
            .thenAnswer(inv -> {
                return (int) inv.proceed() + 4;
            })
            .thenAnswer(inv -> {
                return (int) inv.proceed() + 100;
            })
            .restAnswer(inv -> {
                return (int) inv.proceed() * 100;
            });
        AnNormalService.setStaticInt(2);
        Assertions.assertEquals(6, AnNormalService.getStaticInt());
        Assertions.assertEquals(102, AnNormalService.getStaticInt());
        Assertions.assertEquals(200, AnNormalService.getStaticInt());
        Assertions.assertEquals(200, AnNormalService.getStaticInt());
    }
}