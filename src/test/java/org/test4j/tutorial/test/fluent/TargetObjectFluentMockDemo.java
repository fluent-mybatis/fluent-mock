package org.test4j.tutorial.test.fluent;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.test4j.annotations.Mocks;
import org.test4j.tutorial.test.TargetObjectMockDemo.User;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Mocks(User.class)
public class TargetObjectFluentMockDemo {
    TargetObjectFluentMockDemoMocks mocks = new TargetObjectFluentMockDemoMocks();

    @DisplayName("对指定实例进行mock演示")
    @Test
    void test1() {
        /** 有一个配偶, 2个小孩 **/
        User user = new User("tom")
            .setSpouse(new User("mary"))
            .addChild(new User("mike"))
            .addChild(new User("jack"));
        String hi = user.sayHi();
        /** mock前的自我介绍 **/
        assertEquals("I'm tom and my spouse is mary.I have 2 children, mike and jack.", hi);

        /**
         * 对配偶进行mock
         */
        mocks.User(user.getSpouse()).getName
            .restAnswer(inv -> "virtual " + inv.proceed());
        /**
         * 对小孩进行mock
         */
        mocks.User(user.getChildren()).getName
            .restAnswer(inv -> "fictitious " + inv.proceed());

        hi = user.sayHi();
        /** mock后的自我介绍 **/
        assertEquals("I'm tom and my spouse is virtual mary.I have 2 children, fictitious mike and fictitious jack.", hi);
    }

    @DisplayName("对指定实例进行mock演示")
    @Test
    void test2() {
        /** 有一个配偶, 2个小孩 **/
        User user = new User("tom");
        String hi = user.sayHi();
        /** mock前的自我介绍 **/
        assertEquals("I'm tom", hi);

        /**
         * 对配偶进行mock
         */
        mocks.User(user.getSpouse()).getName
            .restAnswer(inv -> "virtual " + inv.proceed());
        /**
         * 对小孩进行mock
         */
        mocks.User(user.getChildren()).getName
            .restAnswer(inv -> "fictitious " + inv.proceed());

        hi = user.sayHi();
        /** mock后的自我介绍 **/
        assertEquals("I'm tom", hi);
    }
}
