package org.test4j.tutorial.test.fluent;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.test4j.annotations.Mocks;
import org.test4j.tutorial.biz.AnNormalService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@SuppressWarnings({"rawtypes", "unchecked"})
@Mocks(value = {AnNormalService.class,
    org.test4j.tutorial.biz.samename.AnNormalService.class})
public class TimeVerifyMThreadTest {
    TimeVerifyMThreadTestMocks mocks = TimeVerifyMThreadTestMocks.mocks();

    private static final ExecutorService executor = Executors.newFixedThreadPool(5);

    @Test
    public void test_sameNameMock() {
        assert mocks.AnNormalService != null;
        assert mocks.AnNormalService__1 != null;
    }

    @Test(expected = AssertionError.class)
    public void test_multiple_thread2() throws InterruptedException, ExecutionException {
        mocks.AnNormalService().publicMethod.max(2);

        List<Future> futures = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Callable task = () -> new AnNormalService(1).publicMethod();
            futures.add(executor.submit(task));
        }
        int total = 0;
        for (Future future : futures) {
            total = total + (Integer) future.get();
        }
        Assertions.assertEquals(10, total);
    }

    @Test(expected = AssertionError.class)
    public void test_multiple_thread3() throws InterruptedException, ExecutionException {
        mocks.AnNormalService().publicMethod.min(15);

        List<Future> futures = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Callable task = () -> new AnNormalService(1).publicMethod();
            futures.add(executor.submit(task));
        }
        int total = 0;
        for (Future future : futures) {
            total = total + (Integer) future.get();
        }
        Assertions.assertEquals(10, total);
    }
}