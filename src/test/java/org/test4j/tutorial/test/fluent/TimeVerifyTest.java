package org.test4j.tutorial.test.fluent;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.test4j.annotations.Mocks;
import org.test4j.mock.Stubs;
import org.test4j.mock.stubs.StubTest;
import org.test4j.tutorial.biz.AnNormalService;

import java.util.ArrayList;
import java.util.List;

@Mocks({AnNormalService.class, StubTest.IService.class})
public class TimeVerifyTest {
    TimeVerifyTestMocks mocks = new TimeVerifyTestMocks();

    @Test
    public void test_min_normal() {
        mocks.AnNormalService().publicMethod.min(2);
        new AnNormalService(0).publicMethod();
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_min() {
        mocks.AnNormalService().publicMethod.min(2).restReturn(5);
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_min2() {
        mocks.AnNormalService().publicMethod.min(2);
        new AnNormalService(0).publicMethod();
    }

    @Test
    public void test_max_normal() {
        mocks.AnNormalService().publicMethod.max(2);
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_max() {
        mocks.AnNormalService().publicMethod.max(1).restReturn(5);
        new AnNormalService(0).publicMethod();
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_max2() {
        mocks.AnNormalService().publicMethod.max(1);
        new AnNormalService(0).publicMethod();
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_never() {
        mocks.AnNormalService().publicMethod.never();
        new AnNormalService(0).publicMethod();
    }

    @Test
    public void test_between() {
        mocks.AnNormalService().publicMethod.between(1, 3);
        new AnNormalService(0).publicMethod();
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_between_1() {
        mocks.AnNormalService().publicMethod.between(2, 3);
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_between_2() {
        mocks.AnNormalService().publicMethod.between(1, 2);
        new AnNormalService(0).publicMethod();
        new AnNormalService(0).publicMethod();
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_just_1() {
        mocks.AnNormalService().publicMethod.just(1);
        new AnNormalService(0).publicMethod();
        new AnNormalService(0).publicMethod();
    }

    @Test
    public void test_just_3() {
        mocks.IService().method1$.just(2)
            .thenReturn("a")
            .thenReturn("b");
        StubTest.IService service = Stubs.fake(StubTest.IService.class);
        String str1 = service.method1();
        String str2 = service.method1();
        Assertions.assertEquals("a", str1);
        Assertions.assertEquals("b", str2);
    }

    @Test(expected = AssertionError.class)
    public void test_just_2() {
        mocks.AnNormalService().publicMethod.just(1);
    }

    @Test
    public void test_just_normal() {
        mocks.AnNormalService().publicMethod.just(1);
        new AnNormalService(0).publicMethod();
    }

    @Test(expected = AssertionError.class)
    public void test_multiple_thread() throws InterruptedException {
        mocks.AnNormalService().publicMethod.max(2);
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Thread thread = new Thread(() -> new AnNormalService(0).publicMethod());
            threads.add(thread);
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
    }
}